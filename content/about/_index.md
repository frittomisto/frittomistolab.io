---
title: "About Me"
date: 2019-10-29T13:49:23+06:00
draft: false

# image
image: "images/author1.jpeg"

# meta description
description: "this is meta description"

# type
type : "about"
---

🇺🇸️ Italian girl living in California. My parents gave me Giulia as a name, but Starbucks is determined to change it to Julia, so I am not so sure anymore. I am thirty years old but despite my age, you should not take me too seriously.

I like reading and watching TV series so in this blog I give my personal two cents about them. Sometimes I share them in Italian or in English depending on whether I wake up with my right or left foot. The name **Fritto Misto** means literally *Mixed Fried* but we will never speak about cooking. Mainly because, even if I am Italian, my best cooker skill is to able to use Uber Eats. Impressive, I know.

When I am not reading or watching TV, I am working as an applied mathematician researcher. How reading novels and study math are combined in my life is still a mystery even for me. Sometimes you can spot me outside enjoying California's sun. You can recognize me because I am the one complaining because it is too hot but too lazy to move in shadow. I am a peach, I know.

I tried to upload a new post every Sunday, but you can always reach me with [Instagram](https://www.instagram.com/ilmiofrittomisto/) and keep track of what I am reading with [Goodreads](https://www.goodreads.com/user/show/126016636-giulia). I hope you can enjoy this space to share your opinions with me. Have fun :)


---
<p></p>

🇮🇹️ Italiana trapiantata nel Far West (letteralmente visto che vivo in California). All'anagrafe dicono che mi chiamo Giulia e ho trent'anni (quasi), ma nonostante l'età non prendetemi troppo sul serio.

Leggo e vedo serie tv da quando ho memoria e in questo spazio fornisco la mia personale e non richiesta opinione su entrambi. Delle volte lo faccio in inglese e delle volte in italiano, onestamente molto a caso a seconda se mi sono alzata con il piede destro o sinistro. Parlerò un po' di tutto quello che mi va e da qui il nome **Fritto Misto**, perché *pout-pourri* era troppo fancy. Tranquilli però, che di cucina ne capisco meno di zero.

Quando mi avanza del tempo lavoro perché le bollette a fine mese le dobbiamo pagare un po' tutti e al contrario di quello che sosteneva Venditti "la matematica è il mio mestiere". Sono quella che si definirebbe una ricercatrice in matematica applicata. Come questo si concili con la mia passione per la lettura è un mistero anche per me.

Cerco di caricare un post nuovo ogni domenica, ma mi potete sempre cercare su [Instagram](https://www.instagram.com/ilmiofrittomisto/) per parlare con me più spesso o su [Goodreads](https://www.goodreads.com/user/show/126016636-giulia) per prendere nota di quello che sto leggendo. Questo spazio è fatto per esprimere le mie opinioni, ma sarei ben felice di sentire anche le vostre. Spero che comunque alla fine vi abbia intrattenuto :)
