---
title: "Get In Touch"
date: 2019-10-29T13:49:23+06:00
draft: false

# meta description
description: "this is meta description"

# type
type : "contact"
---

🇺🇸️ Do you want to write me? Do you want to share your favorite book or TV show with me? Do you just want to speak about random stuff? Search me on Instagram or write me an email, I will always answer you!

---
<p></p>

🇮🇹️ Vuoi metterti in contatto con me? Vuoi condividere la tua passione per i libri, per le serie tv o per qualsiasi altra cosa che pensi possa interessarmi? Cercami su Instagram o scrivimi una mail, una risposta non la nego mai a nesuno!