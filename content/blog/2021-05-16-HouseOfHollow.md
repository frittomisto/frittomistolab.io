---
title:  "House of Hollow"
date:  2021-05-16T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-05-16-Hollow.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
---

🇮🇹️ Dopo tanto parlare di romance, penso sia giustamente arrivato il momento di cambiare genere. Perchè leggo anche altro, giuro. Ho comprato *House of Hollow* qualche settimana fa solo ed esclusivamente per la copertina spaziale. Si, sono una di *quelle persone lì* che comprano un libro solo per la copertina. Insultatemi. Però a mia discolpa poi i libri li leggo veramente, non li tengo solo per l'arredamento, e House of Hollow è stata un'incredibile sorpresa.

<!--more-->

Non so voi che genere di lettore siate ma io sono una di quelli che va a fasi nel genere che legge. Faccio la dieta a zona della lettura. Ho passato il mio momento gialli, il corrispondente della mia personalissima fase blu di Picasso, il momento scrittori russi, grandi classici, letteratura thriller, letteratura romance, libri fantasy e chi più ne ha più ne metta. Le ho passate un po' tutte. Tranne la fase saggistica che per fortuna ancora riesco a evitare come la peste (segno evidente del fatto che sono ancora giovane e arzilla). Comunque, il punto è che di storie ne ho lette tante, di tutti i generi e quindi adesso è quasi inevitabile che difficilmente riesca a trovare un libro con una trama che mi colpisce. Riesco a trovare tante altre qualità nei libri, ma difficilmente ne trovo uno con una trama poco prevedibile. Probabilmente per il solo fatto di avermi sorpreso House of Hollow merita una menzione d'onore su questo blog. Ma poi è il mio blog quindi faccio un po' come mi pare.

Quando ho iniziato a leggere il libro ho letto la biografia della scrittrice ho scoperta che è l'autrice del romanzo da cui poi è stato tratto *Chemical Hearts*, il film con la star di Riverdale e il protagonista di Dash & Lily. Il film lo potete vedere su Amazon prime video e non è un brutto film, ma non esattamente il mio genere preferito perchè molto più teen di quanto oramai riesco a vedere (contraddicendomi subito con la mia affermazione precedente sull'essere giovane e arzilla). La storia è quella di due ragazzi, Grace e Henry, che si avvicinano lavorando entrambi nel giornale della scuole e sebbene lui sia un pezzo di pane, con una vita tranquilla e tutto sommato nella norma, lei invece è un'altra storia. Sopravvissuta ad un incidente stradale in cui però ha perso la vita il suo ragazzo, nonchè migliore amico, Grace non vive ma sopravvive senza essere mai riuscita veramente a superare e uscire dal suo lutto. Se vi piace il genere guardatelo perchè non è un brutto film. Comunque dopo questa scoperta onestamente ero abbastanza disappointed e non mi aspettavo molto da House of Hollow. Mai aspettativa fu più disattesa.

House of Hollow è la storia di tre sorelle, Grey, Vivi e Iris, che quando sono piccole scompaiono senza lasciare traccia per poi ricomparire un mese dopo nude e ognuna con una cicatrice al collo. Creepy? Non immaginate neanche quanto. Le sorelle non ricordano nulla di quello che le è successo nel mese in cui sono scomparse ma pian piano cominciano a cambiare: capelli bianchi, occhi scuri quasi neri, bellezza incredibile e una abbastanza inquietante capacità di far fare alle persone quello che vogliono loro. Fast forward dieci anni dopo, le sorelle hanno preso strade molto diverse, Grey è stata cacciata di casa dalla madre e fa la supermodella con una sua casa di moda, Vivi è la classica rock star tatuata, sesso droga e rock and roll in giro per l'Europa mentre la diciasettenne Iris conduce una vita tranquilla scuola-casa con la madre. Seppure così diverse le sorelle sono estremamente legate tra di loro, in un rapporto quasi simbiotico e anche abbastanza inquietante. Tutto in questo libro è abbastanza inquietante. Il romanzo segue il punto di vista di Iris che tutto ad un tratto deve affrontare la nuova sparizione della sorella maggiore Grey. Intenzionate a fare di tutto per trovare la sorella scomparsa, Iris e Vivi cominciano una caccia all'uomo che però le porterà a fare i conti e risolvere il mistero della loro scomparsa da bambine. Cosa è successo nel mese in cui sono sparite? è possibile che Grey abbia fatto solo finta di non sapere cose le è successo? Una volta aperto il fatidico faso di Pandora niente sarà più lo stesso per le sorelle Hollow.

> Some people go missing because they want to; some go missing because they’re taken. And then there are the others who go missing because they fall through a gap somewhere and can’t claw their way back.

Onestamente non penso sia possibile identificare un solo genere per questo libro. L'autrice combina atmosfere urban fantasy, horror, mistery, thriller e folklore popolare. Un po' *Twin Peaks* e un po' *Stanger Things* con un pizzico di *Edward mani di forbici*, l'autrice crea questa atmosfera magica, ma al tempo stesso estremamente reale, che ricorda vagamente un sogno che si rivela in realtà un incubo. La bellezza irreale delle sorelle, le creazioni di moda della maggiore e l'atmosfera naturale di fiori e foreste si mischia con odori putridi, rituali quasi satanici e cose morte. Un mix difficile da creare ma che tiene il lettore inchiodato alla lettura e completamente rapito. O almeno, a me ha fatto questo effetto. In questo mix surreale, Krystal Sutherland parla del legame tra tre sorelle, del dolore che devono affrontare e superare per crescere ed arrivare alla fine a poter accettare se stesse. Non solo loro, ma anche la madre dovrà affronare una crescita simile. Come si supera il dolore e il senso di colpa di veder scomparire le proprie figlie davanti ai propri occhi? La gioia di averle ritrovate è abbastanza per superare la paura di perderle di nuovo? La definizione di mamma-chioccia si può veramente applicare in questo caso?

Per quella che è la mia modestissima opinione, vale la pena fare un salto nel vuoto e leggere il libro, farsi trasportare in questa atmosfera magica per scoprire la verità dietro al mistero della scomparsa delle sorelle Hollow. L'unico rischio che incorrete è che la trama vi entrerà sottopelle. Letteralmente.


Peace and love ❤️
