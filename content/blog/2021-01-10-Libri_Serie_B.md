---
title:  "Esistono libri di serie B?"
date: 2021-01-10T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-10-Libri_Serie_B.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "Life style"
tags:
  - "books"
  - "italian review"
---     

🇮🇹️ Quanti di voi hanno nascosto *50 sfumature di grigio* dietro la copertina de *Il nome della rosa*? E quanti una volta scoperti hanno inventato un parallelismo di Anastasia introdotta al mondo sadomaso da Mr Grey e Adso che scopre i piaceri della carne dalla ragazze delle cucine? Quanti alla domanda della commessa sui romanzi rosa nel carrello hanno negato fino allo sfinimento per poi rubare gli Harmony dalla libreria della nonna?
Chiunque nella sua vita di lettore si è sentito giudicato almeno una volta per le sue scelte letterarie, ma esistono veramente libri di serie B?

<!--more-->

Ve lo dico nella maniera più brutale possibile, tolto il dente tolto il dolore, sì, esistono libri di serie B. E vi dirò di più, secondo me rappresentano qualcosa come il 90% del panorama letterario. I capolavori assoluti, quei libri che sarebbero da mettere nella navicella di Superman se esplodesse il mondo, sono veramente pochi se ci pensate. Anche i lettori più accaniti, quelli che magari arrivano a leggere un centinaio di libri in un anno, sanno che probabilmente solo una manciata saranno veramente memorabili. Tutto il resto è un enorme prateria di mediocrità, o pure peggio a volte, che tra un paio di generazioni nessuno ricorderà o leggerà mai. E badate bene che non c'è nessun tono polemico o negativo in questo, anzi.

Secondo me, quando prendete in mano un libro nuovo da leggere ci sono fondamentalmente quattro futuri possibili davanti. Nel primo, il libro in questione è quasi (perchè il polemico che va controtendenza c'è sempre) universalmente riconosciuto come un capolavoro. Può essere un grande classico, un mattone russo, un romanzo contemporaneo o un saggio, qualsiasi genere sia è un libro con una buona reputazione. Il che non vuol dire però che vi piacerà sicuramente, potrebbe benissimo non essere il vostro genere. Ma anche in quel caso, a meno che non siate il polemico di prima, ne riuscite comunque a riconoscere e apprezzare le qualità letterarie. Nel secondo futuro possibile, l'aggettivo migliore con cui descrivere il libro è normale, ma nella sua accezione positiva. Il libro probabilmente rimarrà in qualche classifica letteraria per un paio di settimane, magari mesi, vi farà passare il tempo leggendolo, magari vi piacerà e interesserà anche, ma poi nel giro di qualche anno rimarrà nella libreria dimenticato. *Un bel libro ma niente di che* oppure *carino, ma non il mio genere*. Il terzo futuro mi fa personalmente lo stesso effetto che il fantasma di Marley produce in Scrooge la notte di Natale: un iniziale terrore seguito da una totale indifferenza. Fanno parte di quella categoria tutti quei libri che io definirei impropriamente come trash, ma che vengono spacciati come libri normali. Sono libri che se la credono tantissimo (come se avessero una personalità propria) senza nessun motivo reale. Solitamente questi libri si circondano di sette di ammiratori sfegatati, diretti discendenti dei cacciatori di streghe di Salem, che sono pronti a metterti al rogo se provi a palesare il fatto che il libro sia oggettivamente brutto. Per spiegarvi al meglio questa categoria dovrei fornire esempi concreti, ma preferisco non sfidare la sorte così apertamente. In ultimo ci sono quei libri che io nel tempo ho imparato ad amare tantissimo. Libri che sono trash, sanno di esserlo, sono scritti esattamente con quell intento e vengono venduti come tali. Libri che per un motivo o per un altro potrebbero tranquillamente essere considerati come orrendi, ma che facendolo in maniera così consapevole mi provocano il sentimento opposto. Sono i *Temptation Island* della letteratura moderna.

Capite quindi che le probabilità di leggere libri di seria B sono molto maggiori rispetto a quelle di leggere un capolavoro. Perchè allora vergognarsene e non dichiarare il proprio gusto per l'orrido ai quattro venti? Io credo perchè la vera domanda non è sull'esistenza o meno di libri di una categoria inferiore, ma se scegliere di leggere tali libri ci rende in qualche modo lettori inferiori.

>**Leggere libri di serie B ci renda automaticamente lettori di serie B?**

Io credo assolutamente di no, ma se in teoria può essere un opinione condivisa da molti, in pratica un po' meno. Perchè state pur certi che troverete sempre qualcuno che vi farà sentire lettori di serie B. Nella lettura, come in qualsiasi ambito della vita, gli "altri" faranno sempre delle scelte migliori di voi e sentiranno l'assoluto bisogno di farvelo sapere. Se fate parte di quei dieci lettori nel mondo che non sono mai stati criticati per le loro letture allora probabilmente o siete voi i peggiori giudici di voi stessi o semplicemente nessuno ha mai apertamente espresso il proprio pensiero con voi. Pessimista? Io direi realista, ma dipende dai punti di vista.

Il problema, con i libri, è che il giudizio sui nostri gusti letterari non si basa tanto sulla qualità oggettiva del libro, ma sul pregiudizio che il libro si porta dietro. Se leggi 50 sfumature di grigio in treno, il vicino di posto ti guarda storto non perchè pensa che la caratterizzazione dei personaggi è descritta meglio dietro la scatola dei cereali e quindi tu stai perdendo tempo a leggerlo, ma perchè leggere letteratura erotica è da frustrati. Così come leggere saggi è da intellettuali, leggere fantasy è per i bambini, leggere romanzi rosa è per le casalinghe disperate, leggeri russi è da pesantoni, leggere biografie è da sapientoni. Qualsiasi genere tu stia leggendo, stai pur certo che l'etichetta che si porta dietro ti rimarrà attaccata addosso e auguri nel cercare di liberartene. Sacrilegio pensare di riuscire a trovare tematiche di un certo spessore in un libro fantasy. Eresia dire che i saggi non li leggi perchè ti annoiano. Abominio credere che la sera sul divano tu possa preferire leggere un romanzo rosa piuttosto che vedere un film di Kubrick. Sei direttamente condannato in prigione senza passare dal via quando provi a spiegare che anche le graphic novels sono una forma di letteratura. La faida tra chi preferisce i saggi e chi preferisce i romanzi può essere paragonata solo a quello che si vede all'Olimpico durante il derby Roma-Lazio.

Questo perchè il giudizio che viene dato all'oggetto si trasmette automaticamente alla persona che l'oggetto lo ha scelto. O almeno viene percepito come tale. Ergo libri di serie B vengono associati a lettori di serie B. E questo vale per i libri che leggiamo, i film che guardiamo o il lavoro che facciamo. Esiste un modo per accettare un giudizio in maniera non così personale? Non lo so, alzo le mani sconfitta, quando lo trovate fatemi sapere. Io per ora accetto la realtà dei fatti per cui riesco ad apprezzare Shakespere un giorno e Sylvia Day il giorno dopo. Questo mi rende automaticamente un intellettuale un giorno e una persona frivola il giorno dopo? Può essere, ma almeno le mie multiple personalità riusciranno a tenermi compagnia durante l'ennesimo lockdown. Comunque la pensiate voi, propongo di giudicare di meno e leggere di più, sia che scegliate capolavori assoluti o libri trash.

Peace and love ❤️
