---
title:  "Hotel Del Luna"
date:  2021-02-24T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-02-24/2021-02-24-Personaggi.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "kdrama"
tags:
  - "italian review"
  - "kdrama"
  - "romance"
  - "asian culture"
---

🇮🇹️ Nella traversata che vi porterà ad amare i kdrama tanto quanto me, il secondo drama di cui vi voglio parlare dopo [It is okay to not be okay]({{< ref "/blog/2021-01-31-It_s_okay_to_not_be_okay.md" >}}) è Hotel Del Luna, una bellissima chicca del 2019. La protagonista del kdrama è IU, attrice e cantante a cui inspiegabilmente assegnano sempre il ruolo del mai una gioia (vedi *Moon Lovers: Scarlet Heart Ryeo* o *My Mister*).

<!--more-->

Hotel Del Luna è sicuramente un kdrama con una trama originale. La protagonista della storia è Jang Man-Wol (interpretata da IU) con un ruolo di quello che potrebbe essere l'equivalente di un CEO dell'albergo Del Luna, una struttura nel centro di Seoul che rimane nascosta a quasi tutti gli occhi degli umani.

![image](../../images/post/2021-02-24/2021-02-24-IU.jpeg "IU è Jang Man-Wol.")

L'hotel funziona come una specie di limbo per i  morti che hanno ancora faccende irrisolte o che vogliono aspettare una persona cara prima di passare oltre. Tutti coloro che alloggiano e che lavorano nell'hotel sono quindi fantasmi tranne che per Jang Man-Wol e il manager dell'hotel. Man-Wol si trova in una condizione intermedia tra la vita e la morte, nata umana mille anni prima, si è macchiata di un peccato enorme che la condanna ad essere legata all'hotel fino a quando non avrà espiato la sua colpa. Accogliere i fantasmi nell'hotel e aiutarli a passare oltre per potersi reincarnare è quindi il suo modo per espiare l'aver ucciso un non ben specificato numero di persone quando era in vita. L'hotel è per Man-Wol una redenzione, ma anche una prigione da cui non può scappare.

Il manager dell'hotel, invece, è l'unico umano che serve come da ponte tra il mondo degli spiriti e quello umano. Il suo compito è quello di occuparsi di faccende prettamente da vivi come pagare le bollette, tenere sotto controllo le spese o trattare con i parenti in vita dei morti. La posizione del manager viene quindi occupata ciclicamente da persone differenti a cui Man-Wol fornisce il dono/maledizione di riuscire a vedere i fantasmi. Il giovane Gu Chan-sung si trova costretto a ricoprire tale posizione a causa di un patto che il padre ha stipulato con Man-Wol venti anni prima.

![image](../../images/post/2021-02-24/2021-02-24-Seong.jpg "Yeo Jin-Goo è Gu Chan-sung.")

All'inizio è estremamente riluttante e spaventato, ma si trova costretto a capitolare quando capisce che vedere i fantasmi gli preclude la possibilità di avere un lavoro e una vita normale. Inoltre, quello che alla luce del giorno e per le persone normali è una struttura scialba e insignificante, si trasforma la notte in un albergo magnifico con stanze magiche e ospiti incredibili. Probabilmente tutto sommato vale la pena di lavorare in un posto così.

![image](../../images/post/2021-02-24/2021-02-24-Hotel.png)


In una di queste stanze si trova un giardino con un albero apparentemente secco, senza foglie o fiori. L'albero è il cuore stesso dell'albergo, legato indissolubilmente a Man-Wol e destinato a rifiorire solo quando la condanna di quest'ultima sarà prossima a essere espiata, permettendole quindi di passare oltre e ricevere la possibilità di reincarnarsi. Una specia di rosa della Bella e la Bestia, ma che al contrario fiorisce quando la maledizione è spezzata.

![image](../../images/post/2021-02-24/2021-02-24-Tree.jpg)

Ovviamente l'albero comincia a dare segni di fioritura quando Gu Chan-sung arriva nell'albergo. Puntata dopo puntata vediamo i due protagonisti e gli inservienti dell'albergo avere a che fare con le faccende irrisolte degli ospiti dell'albergo e di loro stessi. La storia più interessante è sicuramente quella di Man-Wol, una storia tragica (altrimenti non avrebbe portato ad una maledizione di mille anni) e piena di rancore. Man-Wol si trova a dover affrontare faccia a faccia tutte le persone che hanno fatto parte della sua vita passata, persone che ha amato e che sono morte e persone che l'hanno tradita, condannata o che le hanno spezzato il cuore. Ci vorrà tutto il suo autocontrollo, e anche qualcosa di più, per non cercare vendetta su queste persone condannando Man-Wol a diventare uno spirito cattivo destinato ad essere eliminato senza reincarnazione. Quello che capiamo piano piano è che Gu Chan-sung è in realtà stato portato nell'hotel da Mago, un'essere superiore che fa le veci di una specie di dio che vuole vedere Man-Wol finalmente in pace e libera dal rancore del suo passato.

![image](../../images/post/2021-02-24/2021-02-24-Personaggi.jpg "Tutti i protagonisti del drama.")

Il drama è una riflessione continua di quello che è il tema della morte e i rancori che ci tengono ancorati e che pesano sulla nostra anima. Pur trattando un argomento così delicato, lo fa in una maniera estremamente ironica e dolce quando necessario. Non viene lasciato quasi mai spazio alla malinconia e alla depressione, complice soprattutto il fatto che i coreani credono nella reincarnazione delle anime e quindi la morte non è mai veramente la fine.

Il personaggio di Man-Wol è secondo me caratterizzato molto bene. Man-Wol ha una personalità all'apparenza (ma non solo) fredda, una testa calda con un brutto carattere, interessata più alle ricchezze che le offrono i vivi che ad aiutare le anime. Gu Chan-sung è esattamente il suo opposto, una persona dolce e disponibile, sempre pronto ad aiutare. Grazie a lui Man-Wol ritrova la sua dolcezza e il suo amore che era stato tradito mille anni prima e quindi sepolto sotto una montagna di rancore. Ovviamente il rapporto tra i due rimane qualcosa che lo spettatore osserva con un fondo di malinconia perchè dalla prima puntata viene messo in chiaro come Gu Chan-sung sarà colui che dovrà aiutare Man-Wo a passare oltre e quindi, letteralmente, a vederla finalmente morire e trovare la pace. Se da una parte quindi sappiamo che lei se lo merita e che è qualcosa che le auguriamo, vedere i due separarsi è estremamente triste e più di una lacrima verrà versata nelle puntate finali. Le atmosfere del drama fanno si che non sia effettivamente un romance a tutti gli effetti. La storia d'amore c'è ed è presente, ma è molto velata e delicata, soprattutto a causa del suo inevitabile destino.

![image](../../images/post/2021-02-24/2021-02-24-Final.png "Ce li abbiamo i fazzoletti per il finale?")

Il drama è un buon equilibrio di ironia e malinconia, leggerezza e tristezza che lo rendono un prodotto estremamente valido a mio avviso. Anche in questo caso, come era stato per [It is okay to not be okay]({{< ref "/blog/2021-01-31-It_s_okay_to_not_be_okay.md" >}}), una menzione d'onore la meritano la colonna sonora e i costumi della protagonista.

Se non vi ho ancora convinto a vederlo, sappiate che vale la pena di vedere le 16 puntate anche solo per la scena finale post titoli di coda. Una cosa molto alla *Marvel* che fa sperare tutti noi fan in una seconda stagione che però per ora non è mai stata confermata. Per descrivere la scena finale dico solo una cosa: Kim Soo Hyun. Ho detto tutto.

![image](../../images/post/2021-02-24/2021-02-24-Final2.jpeg "Allego testimonianza grafica per chi non riconosce Kim Soo Hyun (shame on you).")

Dimenticavo l'ultima informazione fondamentale. Il kdrama lo potete vedere su Viki che è l'equivalente della pentola d'oro alla fine dell'arcobaleno per noi occidentali appassionati di cinematografia asiatica. Andate e vedeteli tutti.

Peace and love ❤️
