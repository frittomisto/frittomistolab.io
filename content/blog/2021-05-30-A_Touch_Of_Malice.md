---
title:  "A Touch of Malice"
date:  2021-05-30T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-05-30-A_Touch_Of_Malice.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "english review"
  - "fantasy"
  - "retelling"
  - "romance"
  - "mythology"
---

🇺🇸️ This is my first English review of a book. Why have I decided to switch language for this review instead of my classic Italian? No particular reason really, I was just so frustrated after reading the book that I thought English readers could be the right audience to complain together about it. Will I switch completely to English as the language of the blog? No, definitely no. I still think my potential (if it exists) is the best express in Italian. Will I write other reviews in English? Yes, probably. Regardless of my origin, I live in the US now and all the books that I read are in English. Still, be gentle, because I am not a native English speaker or writer (Seneca would say that to err is human).

<!--more-->

If this is my first review that you read, I usually write all of them without spoilers (otherwise I warn you in advance). However, this is the third book of the series, so spoilers about the first two books are kind of inevitable (but if you know the myth of Hades and Persephone then you already know the story, can we still talk about spoilers in this case?). As you could have probably guessed no, I did not like the book, but it is totally fine if you have. The whole point of the blog is to share opinions and I especially appreciate opinions that are different from mine. Now that I have made the necessary premises, let's talk about the book.

I honestly had a lot (ok, not a lot, but still some) of expectations about this book. Finally, at the end of the second book, we have seen the potential of Persephone and her magic. Her outbreak at the Forest of Despair was the moment in which I thought she had finally managed to evolve from victim to fighter. So, I expected to see a badass Persephone in this book, and I did not. She did not evolve one bit from the second book, and, if possible, she was even worst, more childish than what I expect from the Queen of the Underworld. It was like she just remembers sometimes that she has some kind of power, but most of the time she cannot control it, neither wants to. She wants to prove herself not as a Goddess but as a person, and so she still tries to conceive herself as a human journalist instead of as Demeter’s daughter. As a result, I think she does not really want to master her power, and still, in this book, she has not understood her potential. For me, this was all really frustrating because I can understand the victim psychology at the beginning, but in the third book, I kind of expect something more, especially after she managed to battle against Hades and Hecate all on her own. Instead, in this book, she sees the destruction that her mother is bringing to the human world, and she does not do anything to try to solve it. Omg, Persephone, you are the Goddess of Spring, do something against the weather and the snow! But no, the strategy of Persephone is to complain with the other Gods that they are not doing enough to find her mother and stop her. Argh, this was so frustrating that I wanted to scream at her. In the end, I think that the fact that I dislike so much the character of Persephone and the whole book is from her POV prevents me to truly enjoy this reading. But there are also other reasons why I did not like the book.

First of all, let’s talk about the story. Nothing happens in the book until the last two chapters. NOTHING. I can see the editorial choice behind this, but it drives me mad that the story does not advance until the very end just to push the reader to buy the next book. Personally, it has on me the exact opposite effect. Now, it’s a matter of principle to not go on with the series. The main focus of this book is the love between Hades and Persephone and let me tell you that it is not a healthy love. These two love each other, they are destined to be together, they are fighting against everyone to be together, and still, in the third book, THEY ARE NOT ABLE TO COMMUNICATE WITH EACH OTHER. The funny thing is that Persephone is very aware that the communication with Hades is not great. Still, she has the gut to give love advice to Apollo and told him that he should speak with his lover to overcome their issues. Really Persephone? In the same way you talk to Hades? He is not willing to share with her anything, to protect her he said, but after growing up under the thumb of Demeter I would expect Persephone to be less inclined to blindly accept people protecting her. Instead, Persephone does not really care, she does not ask, and seems that she does not even care what Hades does during the day. This is particularly evident if you read A Game of Fate that should be the story of A Touch of Darkness from Hades’s POV. Well, Persephone is so blind to what happened to Hades that A Game of Fate is like a completely new book (again, nice editorial choice), and let me tell you that the story is so much better when it is written from his POV. Mainly because Hades has a much stronger personality with respect to Persephone but from her POV the reader is not able to appreciate that (since they do not communicate). I honestly think that the series would be much better if the writer would have alternated the two protagonists' points of view instead of writing different series about the same story. But I can see that we all need to pay our bills at the end of the month and eight books are better than four. So, the question at this point is, if the main focus of the book is the love story and the lovers do not speak with each other, what does happen in the book? Basically, they have sex. Lot of sex. Sex is the answer to everything. Sex is the solution to Persephone’s grief, to Persephone’s trauma, to Persephone’s nightmares, to Hades’s darkness, to Persephone’s almost dying…should I go on? Should I really say out loud that a relationship based only on sex is not a relationship at all? It is definitely a good stress relief, but it cannot be the only one.

The all-sex parade is interspersed with some very brief action scenes that should remind the reader that there is more than one war outside of the lover bubble. There is a war between the Triad and the Gods and there is a civil war between the Gods that see the union between Hades and Persephone as a thread. If these action scenes should have made me appreciate the book, they did not. All the battles are described as Persephone who manages to spur some thorns (every time almost dying in the process) and then is teleported away from Hades. Teleportation is basically the solution to every battle until the very end when the most significant action takes place between Persephone, Demeter and Theseus. At this moment, Persephone has completely forgotten that she can stop time and teleports everyone safe. She could save all of her friends with just these two powers but instead, she just follows Theseus and his plan, asking herself why Hades is not there to solve the problem. I am sorry but this last part was the last straw for me. I really cannot understand Persephone and her choices. If you are reading this mine two cents about the book and you have a different opinion about it, please let me know. I really would like to change my opinion and be able to continue the series.

Peace and love ❤️
