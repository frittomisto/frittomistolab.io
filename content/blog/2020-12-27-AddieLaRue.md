---
title:  "La vita invisibile di Addie La Rue"
date: 2020-12-27T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2020-12-27_addie_1.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "young adult"
  - "fantasy"
---
🇮🇹️ Non capita quasi mai che prima di leggere un libro io cerchi recensioni online
o legga post sul libro stesso. Di solito lo faccio dopo che l'ho finito,
soprattutto come modo per combattere la malinconia che accompagna sempre
il mettere la parola fine a qualcosa.
Tuttavia, con "The invisibile life of Addie La Rue" è successo qualcosa di strano.
Più vedevo video e recensioni sul libro, meno ero d'accordo e più mi arrabbiavo
e sentivo il bisogno di dire la mia. Quindi, here we are,
a fornire la mia personalissima e non richiesta opinione su questo tanto dibattuto libro.

<!--more-->

Premettiamo due cose. First of all, questo è il mio primo libro della Schwab che leggo,
non ho confronti da fare con gli altri suoi romanzi e non so se questo
sia il suo solito stile letterario o no.
Non so se questo sia il suo miglior romanzo perchè non conosco gli altri.
Secondo, io sono arrivata tardi (as usual, direbbero alcuni) a leggerlo e
non ho vissuto l'incredibile hype che si era creato intorno al romanzo
quando è uscito (dormivo? mangiavo? guardavo le nuvole? chi può dirlo...
io comunque me lo sono perso). Questo sembra essere stato un fattore
determinante visto che la maggior parte del popolo di booktube Italia
critica il libro perchè pubblicizzato come il migliore del 2020,
la chicca dell'autrice, il meglio del meglio ed altri megalomanismi simili
che poi (ovviamente) vengono delusi (perchè dai, siamo onesti, le cose con un
enorme hype che poi non si sono rivelate delusioni enormi si possono contare
sulle dita della mano anche se di nome fai Capitan e di cognome Uncino).
Io ho iniziato il libro senza pretese, avevo letto la sinossi su Amazon,
mi aveva ispirata, ero in una fase di acquisti compulsivi e quindi l'ho preso.
E l'ho letto. E mi è piaciuto. BOOOOOM.

Si, l'ho detto e ora lo ripeto anche.
```
Ho letto "The invisible life of Addie La Rue" e mi è piaciuto.
```
Linciatemi, sono pronta.
Certo, per carità, non penso sia il romanzo del secolo,
non è un capolavoro che se non leggi ti perdi la pietra miliare dello young adult
però l'ho trovato un bel romanzo. Godibile. Probabilmentre prima o poi lo rileggerò anche.
C'è tanta merda da leggere in giro e questo sicuramente non è tra quelli
(e io me ne intendo perchè di merda ne leggo parecchia e con una certa soddisfazione).

<!--![image](../../images/post/2020-12-27_addie_1.jpeg) -->

Iniziamo dalla trama. Adeline è una ragazza di 23 anni che vive in un paesino
rurale della Francia del 1714. Da lei qundi ci si aspetta una cosa soltanto,
che si sposi, faccia dei figli, si faccia poche domande e non sia curiosa.
Adeline tuttavia è esattamente questo, una ragazza curiosa,
uno spirito libero diremmo noi, affamata di quello che la vita ed
il mondo potrebbe offrirle se gliene fosse data la possibilità.
Peccato che questa possibilità non le viene data, ma anzi il suo spirito
viene schiacciato e contenuto in tutti i modi possibili.
E già con questa premessa tutto il mio animo femminista era stato stuzzicato
a sufficienza ed ero pronta a dichiarare guerra a chiunque stesse propinando
ad Adeline il matrimonio come scopo nella vita.
Comunque, al culmine della disperazione, senza più possibilità di salvezza,
Adeline scappa di casa e prega gli antichi dei di liberarla.
Bhe, qualcuno risponde alla chiamata di Adeline ma non è un dio amichevole.
Chiamatelo diavolo, chiamatelo spirito oscuro,
il nome può essere diverso ma il concetto è quello.
Adeline fa un patto con lo straniero: essere libera come desidera in cambio
della sua anima quando si sarà stancata di vivere.
Praticamente un _Faustian bargain_ versione emancipazione femminile.
Il rovescio della medaglia (perchè c'è sempre un cavillo che ti frega in tutti i contratti) è che l'essere libera si traduce per Adeline in una vita immortale in cui però non viene riconosciuta e ricordata da nessuno. Una vita fatta di primi incontri anche se per Adeline primi incontri non sono, una vita in cui non puoi possedere nulla, in cui anche solo cercare di dormire in un ostello diventa impossibile se chi ti ha dato la camera poi non si ricorda più di te e ti caccia via. Sostanzialmente una vita di solitudine ma nel senso più estremo del suo termire. Adeline non possiede niente e non ha nessuno e non potrà mai averli. Tranne ovviamente lo straniero con cui ha stretto il patto, unica costante di una eterna vita di incertezze. Il romanzo alterna capitoli in cui viene descritta la vita di Adeline dopo il patto, un Adelina fondamentalmente in balia degli eventi, spaventata, maltrattata, mal nutrita e chi più ne ha più ne metta, e la vita di Adeline nella New York moderna, un Adeline che ha vissuto già 300 anni e che quindi, volente o nolente, ha imparato a vivere al meglio delle sue possibilità. Nella New York del 2014 Adeline incontra per caso un ragazzo, Henry, che si ricorda di lei, ricorda il suo nome e quindi ovviamente (anche perchè se no non ci sarebbe un romanzo) è destinato a cambiare le carte in tavola.

Descritto così è facile aspettarsi dal romanzo un romance
dove lei dannata incontra lui che sfugge alla maledizione e la salva.
Ecco no. Non fatelo. _The invisible life of Addie La Rue_ è tutto tranne che un romance.
Se cercate un libro sull'amore che rompe le malidizione allora cambiate, leggete altro.
Ci sono decine di rivisitazioni di _La Bella e la Bestia_ e questo romanzo non è tra quelle.
Adeline è un personaggio forte, testardo e cocciuto ai limiti della sopportazione,
ha vissuto 300 anni affrontando la fame, il freddo, la solitudine e le guerre.
Secondo voi ha bisogno che un new yorkese del ventunesimo secolo la salvi?
No, e infatti non è quella la storia.
Molte critiche che ho letto si lamentano del fatto che il romanzo non abbia una trama.
Non è vero secondo me, non è sicuramente un romanzo in cui ci sono eventi eclatanti, non ci sono battaglie, non c'è la storia d'amore del secolo, non c'è il plot twist che ti fa tenere il fiato sospeso. Nel romanzo si segue la vita di Adeline, come la affronta, quali sono le sue difficoltà, le sue paure, la sua solitudine. Vediamo Adeline crescere e adattarsi, la vediamo sbagliare e soffrire ma la vediamo anche andare avanti, un passo alla volta, una forma di resilienza derivante dalla testardaggine e dal desiderio di libertà che la spinge a rialzarsi sempre. Non è propriamente un romanzo introspettivo ma quasi. Ma quindi perchè ti è piaciuto se non succede nulla? Perchè secondo me la relazione tra Adeline e Luc, lo straniero con cui stringe il patto, vale tutto il libro. Durante il romanzo vediamo l'evoluzione della relazione tra i due ed è qualcosa di incredibilmente affascinante. Adeline e Luc sono due magneti destinati a scontrarsi, due opposti destinati a distruggersi e salvarsi per poi distruggersi di nuovo (ripetete insieme a me: non è un romance, non è un romance, non è un romance...). La relazione tra i due è una relazione tossica, uno squilibrio di potere ai massimi livelli, Luc cerca in tutti i modi di piegare Adeline, di spezzarla per avere la sua anima e più ci prova e più lei va avanti, con l'assoluta certezza di non volerla dargliela vinta mai, anche a costo di perdere se stessa nel farlo. Tuttavia i due sono uno l'ancora di salvezza dell'altro, sono due esseri solitari in un mondo dove nessuno li conosce tranne se stessi e l'altra persona. E chi nella vita non vuole essere solo? Onestamente ho adorato vedere l'evoluzione tra i due e quindi probabilmente di riflesso questo mi ha fatto amare meno il personaggio di Henry. L'ho trovato onestamente abbastanza scialbo ma in un romanzo con due personaggi su tre con una personalità così tanto forte, era scontato che il terzo ne risultasse schiacciato.

Una critica generale che posso fare al romanzo è la presenza di certe frasi ad effetto scritte in giro con il solo scopo di essere citate nella didascalia del post X su instagram. Ci sta, il classico se lo fanno tutti perchè non io, e alla fine del mese le bollette le dobbiamo pagare tutti e se per farlo bisogna buttare qualche frase d'effetto qua e là non vedo perchè no. Però poi nel leggerle diventano un po' forzate e fanno perdere qualcosa. Nel complesso il mio voto al romanzo è di 4/5. Non è il romanzo del secolo, probabilmente la caratterizzazione dei personaggi poteva essere fatta meglio, il personaggio di Henry l'ho trovato ai limiti dell'inutile, ma ho amato la testardaggine di Adeline e sono stata affascinata dalla relazione tra Adeline e Luc. Ho amato il fatto che Adeline non si penta mai della scelta che ha fatto. Il patto con Luc l'ha condannata ad una vita che molti definirebbero d'inferno ma Adelina ha l'assoluta certezza che l'alternativa sarebbe stata molto peggiore ed è assolutamente determinata a godersi tutto il possibile con i mezzi che ha. Praticamente è la versione in carta e inchiostro del classico "attenta a quello che desideri perchè poi potresti ottenerlo" mischiata a "quando la vita ti da limoni, facci una limonata".

Secondo me una possibilità se la merita.

Peace and love ❤️


