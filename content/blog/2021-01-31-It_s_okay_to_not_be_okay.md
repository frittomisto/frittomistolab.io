---
title:  "It's okay to not be okay"
date:  2021-01-31T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-01-31/Cupa.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "kdrama"
tags:
  - "italian review"
  - "kdrama"
  - "romance"
  - "asian culture"
---

🇮🇹️ Benvenuti nel mondo dei korean dramas. Impero del romanticismo più cheesy possibile, fatto di lunghe scene di sguardi, amori predestinati e triangoli amorosi. Il genere dei kdramas non esiste al di fuori della Corea del Sud, non esiste un equivalente nella produzione cinematografica italiana o americana, quindi se non ne avete mai visto uno lasciate che vi introduca in questo mondo. Oggi siamo qui per parlare di quello che per me è stato IL kdrama del 2020, ma anche per farvi diventare tutti potenziali neofiti, se non dipendenti, da questo mondo.

<!--more-->

Quando dico che guardo i kdramas le reazioni di solito si dividono in due categorie: chi ha visto *Parasite* e quindi pensa che guardo solo thriller psicologici e chi della Corea del Sud conosce solo Gangnam Style e quindi pensa che guardo telefilm demenziali di un genere non ben identificato. Come al solito la realtà non è nessuna delle due cose (ma spesso anche entrambe).

I korean dramas sono produzioni televisive dai generi più disparati (storici, action, thriller, horror, comedy...), solitamente la storia si sviluppa in 16 episodi da circa un'ora ciascuno ed è estremamente raro che una serie venga portata avanti per più di una stagione (cosa che apprezzo tantissimo). Nonostante i diversi generi, la componente romance è solitamente ben presente, magari delle volte è meno centrale, ma spesso ne fa da protagonista. La storia d'amore tra i personaggi attraversa tipicamente queste tappe: un incontro in giovane età tra i protagonisti che poi si perdono di vista e si ritrovano adulti (spesso connesso con un amore predestinato), un amore che viene solitamente ostacolato da qualcosa o qualcuno (molto spesso una famiglia facoltosa), un triangolo amoroso con conseguente friendzonata di turno e un finale molto lieto o molto tragico (spesso però con risvolto positivo nella vita successiva). Rispetto a quello che siamo abituati a vedere nella cultura occidentale, l'amore nei kdramas è estremamente casto, un bacio tra i due protagonisti equivale ad un profondo impegno da parte di entrambi ed è il trasporto più esplicito che potrete vedere nel drama (al massimo si potrà far intendere una scena di sesso, ma mai farla vedere). Ovviamente ci sono anche kdramas che non contengono romance, ma da quel che ho potuto vedere io non sono la regola quanto l'eccezione. Nonostante questa premessa possa farvi immaginare serie tv abbastanza frivole (e molto spesso lo sono), in realtà ci sono molti temi seri che vengono affrontati con questa impostazione più leggera. *It's okay to not be okay* è uno di questi. Come il titolo suggerisce, il tema principale è quello delle malattie mentali, dello stigma sociale che le contaddistingue e delle difficoltà a cui i pazienti e le persone che gli stanno intorno vanno incontro. Il kdrama è stato trasmesso per la prima volta nel 2020 e in Italia lo potete trovare sottotitolato su Netflix (se i sottotitoli sono uno scoglio per voi mettetevi l'anima in pace perchè non troverete mai kdramas doppiati in italiano o in inglese, per fortuna).

*It's okay to not be okay* segue la storia di Moon Gang-tae, un infermiere in un istituto psichiatrico con un carattere estremamente calmo, empatico e gentile dovuto sicuramente al lavoro che svolge, ma anche alla sua situazione familiare. Moon Gang-tae, infatti, vive da solo con il fratello maggiore Moon Sang-tae di cui si deve prendere cura in quanto affetto da autismo. I due fratelli sono spesso costretti a muoversi di città in città per sfuggire ad incubi ricorrenti che affliggono il fratello maggiore sin da quando è stato l'unico testimone dell'omicidio della madre. Una vita quindi sicuramente non facile di cui Moon Gang-tae ha il peso di tutte le responsabilità. Il quadro familiare viene improvvisamente scombussolato dalla presenza di Ko Moon-young, una famosa scrittrice di favole per bambini che sembra affetta da un disturbo della personalità antisociale. Per una serie di circostanze, i tre si ritrovano a lavorare all'ospedale psichiatrico di Seongjin City, cittadina che ha dato i natali a tutti e tre i protagonisti. Le vite dei personaggi si intrecciano a quelle dei pazienti e del personale dell'ospedale. Nel corso della storia i tre protagonisti dovranno affrontare, spesso in modo molto doloroso, le ferite e i drammi del loro passato per poter andare avanti nelle loro vite.

<!-- <img src="{{site.baseurl}}assets/posts/2021-01-31/Protagonisti.jpeg" alt=""> -->
![image](../../images/post/2021-01-31/Protagonisti.jpeg "Moon Gang-tae, Ko Moon-young e Moon Sang-tae.")

Questo kdrama è una chicca, non c'è altro modo per definirlo. Uno dei migliori che io abbia mai visto e nell'ultimo anno ne ho visti parecchi. C'è la componente romance che però non è mai frivola o sdolcinata, passatemi il termine. I due protagonisti sono persone emotivamente distrutte, aprirsi l'uno all'altro è un processo molto lento e doloroso influenzato prima dal carattere superficialmente duro di entrambi e poi dai fantasmi del passato che li affliggono. Bellissimo è anche come viene trattato l'amore tra i due fratelli che non è un amore perfetto, ma profondamente segnato dall'autismo del maggiore e dallo squilibrio che questo comporta. Squilibri che hanno radici profonde fin da quando erano piccoli con la madre ancora viva e che riusciranno ad essere superati solo quando entrambi saranno pronti a fronteggiarli. Una cosa che apprezzo molto, nei kdramas in generale, è che non c'è vergogna nel mostrare i sentimenti più intimi anche per la sfera maschile. L'uomo se soffre piange e lo da a vedere, non se ne vergogna e non lo nasconde, ciò non lo rende meno uomo o meno maschile (cosa che invece raramente si nota nelle produzioni cinematografiche occidentali). Infine quello che vediamo rappresentato è l'amore tra un fratello e una sorella, Moon Sang-tae e Ko Moon-young, che sebbene non siano imparentati da legami di sangue riescono a dare al termine *famiglia* un significato del tutto personale.

![](../../images/post/2021-01-31/Poster.jpg "L'atmosfera magica e fiabesca è velatamente presente in tutto il kdrama.")


Come probabilmente ci si può aspettare da un kdrama sulle malattie mentali alcuni tratti sono anche estremamente dark, con un' ambientazione cupa che perfettamente riflette la gabbia mentale che imprigiona i personaggi. Questa ambientazione segue soprattutto il personaggio di Ko Moon-young che è quella in cui le ferite emotive sono meno evidenti al mondo esterno e quindi probabilmente anche più profonde. Infatti, sebbene i problemi dei due fratelli siano quasi palesi per tutti, il passato di Ko Moon-young è qualcosa che lei non rivela mai a nessuno, qualcosa che nasconde dietro una facciata di donna estremamente elegante e sicura di se.

![](../../images/post/2021-01-31/Cupa.jpg "Ko Moon-young è tra tutti e tre quella che è imprigionata nell'ambientazione più cupa.")


In contrapposizione alle atmosfere con un richiamo più dark, il kdrama intervalla delle scene molto fiabesche che riprendono le storie per bambini che scrive Ko Moon-young. Per i temi trattati è difficle identificare queste storie come le classiche favole per bambini, ma anzi vengono spesso considerate come troppo crude e violenti. In realtà in ognuna Ko Moon-young mette un pezzo di se stessa e un messaggio finale che solo chi la conosce veramente riesce a comprendere.

![gif](../../images/post/2021-01-31/Gif_ISOTNBO.gif)

Tutte queste atmosfere vengono perfettamente rappresentate da una colonna sonora spettacolare che riesce a dar voce a tutte le emozioni che vengono espresse dai personaggi. Io vi consiglio di ascoltare tutte le tracce del soundtrack, ma canzoni come *Got you*, *Wake up* o *I'm your Psycho* sono particolarmente evocative.

Per finire, una nota a parte va fatta sulla scelta dei costumi per la protagonista. Io di solito è una cosa che noto poco perchè di moda ne capisco meno di zero, ma in questo caso bisogna essere ciechi per non apprezzare i costumi di Ko Moon-young.

![](../../images/post/2021-01-31/Vestiti.jpg "L'attrice Seo Yea-ji porta tutti i vestiti magistralmente.")

Quindi, state andando a recuperare la serie vero?

Peace and love ❤️
