---
title:  "Lore"
date: 2021-01-13T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-13_lore.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
  - "mythology"
---     

🇮🇹️ Le premesse di *Lore*, l'ultima uscita young adult di Alexandra Bracken, erano una storia interessante e una copertina spaziale. Molti che lo avevano letto come ARC lo descrivevamo come un Hunger Games intriso di mitologia greca e la cosa mi aveva talmente tanto incuriosito da spingermi addirittura a preordinarlo. Adesso che ho finito di leggerlo posso darvi il mio personale e non richiesto parare se ne sia valsa la pena. Come al solito, niente spoiler.

<!--more-->

Devo ammetterlo, il binomio mitologia greca e Hunger Games mi avrebbe conquistato a prescindere. La mitologia greca, così come tutta l'epica in generale, mi ha sempre affascinato moltissimo, forse perché semplicemente mi piacciono le storie e quale miglior storia se non quella di eroi e dei e delle loro gesta leggendarie? Entrambi i libri di Madeline Miller, *la canzone di Achille* e *Circe*, sono in assoluto due dei migliori libri che io abbia mai letto (spoiler: leggete quelli non questo). Nel caso di Lore, c'è il twist Hunger Games che come serie ho sempre trovato estremamente valida se facciamo finta che il terzo libro non sia mai esistito. Comunque, l'idea dell'arena e dei campioni che si devono uccidere a vicenda per decretare il vincitore è tanto brutale quanto vincente (almeno per quanto riguarda romanzi e film, vero Russell?). Di solito almeno.

Nel romanzo di Alexandra Bracken gli Hunger Games sono chiamati Agon e si svolgono una volta ogni sette anni nel mondo reale. Agon è la punizione che Zeus infligge a nove dèi (Ermes, Afrodite, Efesto, Poseidone, Ares, Dioniso, Artemide, Apollo e Atena) colpevoli di aver cercato di spodestare Zeus dopo che lui gli aveva fatto notare come scatenare il caos o mandare pestilenze non era probabilmente un piano troppo smart per riottenere l'adorazione degli umani. E tutti sanno che puoi far incazzare tutti ma non il re dell'Olimpo. Infatti, durante gli Agon, per una settimana i nove dèi sono costretti a risvegliarsi in un corpo umano, che quindi può essere ucciso, e vengono cacciati dai discendenti di nove potenti casate (i discendenti di Achille, di Odisseo, di Teseo, di Cadmus, di Perseo...). Colui che riesce ad uccidere un dio allora ne acquista automaticamente i poteri e l'immortalità, diventando un nuovo dio che porterà gloria, potere e ricchezza alla casata di cui fa parte. Se uccidi Ermes, ad esempio, puoi fondare il nuovo Amazon e diventare miliardario consegnando pacchi, se uccidi Afrodite puoi puntare a diventare la nuova Kim Kardashian e cercare il tuo Kanye personale. Tuttavia, come ago della bilancia, il nuovo dio passerà da essere predatore ad essere preda nel ciclo successivo, destinato ad essere cacciato a sua volta dalle casate rivali nel nuovo Agon. Un gioco al massacro di cui non sembrano esserci mai vinti o vincitori, ma a cui tutti si prestano ben volentieri alla ricerca di una gloria seppur momentanea (pare che nel presente del romanzo il concetto di hubris non sia pervenuto).

> If there were once heroes, they are all gone now. Only the monsters remain.

Il ciclo che seguiamo nel romanzo si svolge a New York e dei nove dèi originali e delle nove casate ne sono rimasti molti pochi. Lore, la nostra protagonista, è una diciassettenne ultima sopravvissuta della casata di Perseo, dopo che il ciclo precedente ha visto la morte violenta di entrambi i genitori e delle sorelle. Il lutto ha sconvolto così tanto la ragazza che ha quindi deciso di tagliarsi fuori dal mondo degli dèi e cacciatori e vivere una vita normale. Sennonché, all'inizio del nuovo Agon, rifà la sua comparsa nella vita della ragazza Castor, compagno di allenamenti di Lore quando erano bambini e uno dei figli della casata di Achille. Castor in teoria doveva essere morto alla fine dell'Agon precedente, ma invece eccolo vivo e vegeto sette anni dopo che chiede a Lore di seguirlo manco fosse il bianconiglio con Alice. Visto che i guai girano sempre in coppia, nella stessa giornata Lore si ritrova Atena mezza morta sullo zerbino di casa. La dea le chiede aiuto nella speranza di porre una fine definitiva alla punizione del padre e in cambio le promette vendetta per la morte della famiglia. Lore si ritrova quindi nuovamente coinvolta nelle trame del gioco e farà tutto il possibile per vendicare la famiglia, riconquistare l'onore del suo nome e non perdere sé stessa.

Quindi è un bel libro? Ehhhhhh, nì. L'idea della trama è molto bella, ma secondo me la messa in pratica manca decisamente di sostanza. Il libro è uno young adult, ma nella sua versione più young che adult. Perché poi da adulto pretendi delle cose da un romanzo come una buona caratterizzazione dei personaggi (o perlomeno dei personaggi interessanti), pretendi un cattivo di turno con una certa sostanza (non un esaltato che poi basta toccarlo con un dito per romperlo) e pretendi uno sviluppo della trama con un senso logico (il tutto non può risolversi magicamente senza motivo). Insomma, da adulto diventi abbastanza rompipalle, ma se sei nella tua versione teen allora è un libro carino, non è scritto male e ti può intrattenere. Se invece cerchi qualcosa che vada oltre alla superfice della trama, allora lascia perdere. L'unico personaggio che l'autrice cerca (senza riuscire a mio avviso) di caratterizzare in maniera completa è Lore che però non è la persona più amabile del mondo. Se tu puoi fare una scelta sbagliata stai pur certo che lei ne farà almeno due e invece che imparare dai suoi errori persevererà per fare la scelta sbagliata numero tre. Ottimo, brava ragazza. Inoltre, come persona manca decisamente di consistenza, non fa in tempo a decidere di non partecipare più all'Agon che poi al ciclo successivo ci torna dentro in neanche 24 ore (non dico che dobbiamo essere coerenti sempre, ma fingiamo almeno all'inizio). Atena poteva essere un personaggio bomba, una dea nata per essere cacciatrice che si ritrova condannata dal padre ad essere cacciata. Ovviamente però non le si rende giustizia per nulla, ho trovato la sua evoluzione abbastanza inconsistente e il suo rapporto con il padre e la punizione dell'Agon non viene mai approfondito. Salvo poi però diventare determinante nelle scelte finali della dea. Ma come? Quando siamo passati da guerriera protettrice delle città a figlia in cerca dell'amore paterno? Per carità possiamo anche diventarlo, ma qualche motivazione durante il percorso me la devi fornire. In generale la scelta di Zeus, nonostante sia il punto di origine di tutto, viene sempre trattato in maniera molto superficiale. Era una punizione? Era un test? Era tutto uno schema del Fato? Zeus ha continuato a sorvegliare i giochi o no? Si potevano prevedere infinite scelte di trama migliori rispetto al nulla cosmico.

Il personaggio maschile, Castor, è il classico bravo ragazzo, quello che non è mai arrabbiato, non alza mai la voce, non mette mai in dubbio la buona volontà delle persone, non vuole uccidere nessuno. Castor è talmente tanto candido da essere quasi fastidioso, soprattutto se lo pensiamo nel contesto in cui si trova, in cui in teoria per sopravvivere dovresti essere ben l'opposto. Infine, quello che dovrebbe essere il personaggio del cattivo non merita di essere commentato, non penso che l'autrice ci si sia applicata troppo quindi chi sono io per farlo?

Comunque, alla fine il libro secondo me si è meritato 3/5, per il semplice fatto che ha una copertina incredibilmente bella. Cosa c’entra Medusa nella storia? Perseo, nella mitologia greca, è soprattutto ricordato per l'uccisione di Medusa, donna bellissima che viene tramutata da Atene in Gorgone dopo essere stata violentata da Poseidone in uno dei suoi templi. La parte del romanzo dove Atena spiega a Lore le sue ragioni dietro la punizione di Medusa è quella che mi è piaciuta di più in assoluto. Capite però che è una delusione se dopo 460 pagine ne apprezzate solo un paragrafo.

Peace and love ❤️
