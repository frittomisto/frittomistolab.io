---
title:  "Steve Jobs non abita più qui"
date: 2021-01-24T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-24_california.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "reportage"
  - "Life style"
  - "American dream"
  - "USA"
---  

🇮🇹️ Per tutti coloro che si stavano chiedendo se leggo solo romanzi (se ve lo stavate chiedendo veramente probabilmente è il momento di ampliare il vostro orizzonte di intrattenimenti), la risposta ovviamente è no. *Steve Jobs non abita più qui* è una specie di  collage esteso e approfondito di articoli sulla California pubblicati negli anni su *Il Foglio*. Frutto di un erasmus tardivo di Michele Masneri nella San Francisco del 2016, il giornalista ci fornisce aneddoti di ogni tipo sul presente e il passato di quello che letteralmente è il mondo del Far West.

<!--more-->

Il libro di Masneri non poteva che iniziale con l'episodio più sconvolgente nella vita del californiano medio nel 2016: l'elezione di Trump. Masneri parla e intervista uno svariato panorama demografico, ognuno con le proprie opinioni su come Trump sia arrivato a vincere le elezioni. A me ha ricordato un po' il clima che c'era in Italia quando ha vinto Berlusconi nel 2008: con chiunque parlavi non lo aveva votato nessuno, ma non si sa come aveva vinto lo stesso. La California comunque l'elezione di Trump non l'ha presa benissimo. Stato storicamente molto democratico, nelle ultime elezioni era stato affidato a Biden prima ancora della chiusura dei seggi. L'impatto quindi che le elezioni del 2016 hanno avuto su questo luogo non è stato marginale, ma anzi Masneri ci racconta come sia stato argomento di discussione e di sconcerto per parecchio tempo. Io personalmente sto vivendo la California nel momento diametralmente opposto e vi posso dire che anche dopo 4 anni il sentimento di sconcerto verso il presidente uscente non è mai finito. Nelle giornate successive al primo dibattito tra Trump e Biden, infatti, molti addirittura si scusavano con me per come era stato rappresentato il loro paese (pivelli, pensavo io, non sopravvivereste due giorni a vedere un programma qualsiasi della tv italiana).

![image](../../images/post/2021-01-24/CalTrain.jpeg "Il Caltrain è il servizio ferroviario che attraversa parte della California da San Jose a San Francisco.")

Attraverso incontri, interviste e aneddoti, Masneri va avanti  a raccontare le mille sfaccettature che uno stato come la California ha da offrire. Primo fra tutti sicuramente l'incubo immobiliare che affligge tutti indiscriminatamente. Comprare casa in California è probabilmente molto meno probabile di riuscire a vincere la lotteria. I prezzi degli immobili sono qualcosa di totalmente assurdo, gonfiati dal fatto che il terreno vale più dell'oro e chi lo possiede ha in atto una guerra feroce per evitare il deturpamento del paesaggio con costruzioni edilizie incontrollate. Non è anomalo quindi che dirigenti o impigati che guadagnano decine di migliaia di dollari ogni mese poi si ritrovano a dormire in camper o nelle loro Tesla fuori dalle aziende o occasionalmente su divani di comuni più o meno promiscue. San Francisco diventa così la capitale anche dei senzatetto più sfortunati che però per la maggior parte riescono ad organizzarsi con tende improvvisate (tipo quelle del Decathlon che si aprono quando le lanci) che popolano i marciapiedi di certi quartieri della città. Al polo opposto, Masneri intervista anche il designer David Kelley (famoso soprattutto per il design del primo mouse Apple), amico dell'architetto italiano Ettore Sottsass con cui ha disegnato la sua umile dimora da 8mila square foot nella Silicon Valley. La casa è adesso in vendita alla modica cifra di 15 milioni di dollari, ma vi consiglio di cercarla su Google perchè è una vera chicca. Se qualcuno ha 15 milioni di dollari da regalarmi per comprarla telefonare ore pasti grazie.

![image](../../images/post/2021-01-24/PaintedLadies.jpeg "Le Painted Ladies di San Francisco sono costruzioni vittoriane dai colori pastello e tappa fissa del turista medio.")

![image](../../images/post/2021-01-24/FinancialDistrict.jpeg "L'edificio a sinistra è la Trasamerica Pyramid ed è il secondo grattacielo per altezza della città di San Francisco. L'edificio a destra, che per chi è stato a New York ricorda una versione in miniatura del Flatiron Building, è la Columbus Tower, nota anche come Sentinel Building.") 

Masneri ci racconta anche di come San Francisco è stata ufficialmente certificata nel 1964 come "la capitale gay d'America" dove la cultura queer negli anni ha potuto trovare sfogo e mettere radici. E ovviamente come qualsiasi cosa che mette radici in California ne nasce un business intorno come quello delle palestre per gay, saune per gay, ma anche un impero di aziende che producono condom in svariate tipologie e pillole antivirali che sostituiscono le caramelle nella lotta contro l'AIDS. La nascita di nuovi business in California, infatti, può essere considerata una sorta di religione. Questo stato rappresenta da solo quasi un quinto del PIL americano e ha fatto del progresso tecnologico la sua ragione di vita. Il californiano medio lavora in una startup, ha creato una startup o ha un progetto per una startup, spesso anche le tre cose combinate. Questo stato ha visto nascere nel raggio di poche decine di kilometri aziende dal fatturato milionario come Google, HP, Apple, Facebook, Tesla o le più "modeste" Uber e Airbnb, solo per citarne alcune. La Silicon Valley, infatti, affonda le sue radici nella dicotomia università-startup dove nei due poli principali di Stanford e Berkley l'innovazione tecnologica viene stimolata e trova terreno fertile per crescere. Tra l'altro, fun fact, Masneri racconta di come l'università di Stanford esista oggi grazie al fatto che l'unico figlio dei coniugi Stanford morì di tifo nel 1884 durante una gita a Napoli. Non avendo più un erede a cui lasciare l'impero produttore di albicocche, i coniugi decidono di trasformare la fattoria in una facoltà di agraria, lanciati da un instinto paterno che viene elargito a più figli possibili. Possiamo dire quindi che l'univeristà che negli anni ha sfornato premi Nobel, presidenti e future aziende miliardarie esiste solo grazie all'Italia? Probabilmente non lo possiamo dire, ma io lo dico lo stesso.

![image](../../images/post/2021-01-24/Stanford.jpeg "La famosa Memorial Court di Stanford con al centro la chiesa con la facciata dorata eretta dai cattolici coniugi Stanford.")

Ovviamente l'altro ago della bilancia è che se il californiano medio è uno _startupparo_ della Silicon Valley, allora in tempo di pandemia il territorio si trasforma in un enorme deserto tecnologico. Non più costretti a subire la gogna immobiliare del posto, infatti, i dipendenti delle grandi aziente possono permettersi di lavorare comodamente con il loro mac book pro dal divano di casa, dove casa può essere l'Arizona, l'Ohio o qualsiasi posto oltreoceano. Ne va di pari passo che quindi un territorio fondato sul soddisfare i fabisogni di questa entità diventata mitologica vada inevitabilmente in crisi quando l'oggetto del desiderio sparisce. Per questo invidio molto la California che Masneri descrive nel romanzo, molto lontano da quella che è la California attuale in tempo di pandemia.

![image](../../images/post/2021-01-24/Facebook.jpeg "Il campus di Facebook che nella foto scorgete tra le fratte è adesso un enorme complesso dal colore turchese circondato da un enorma parcheggio quasi completamente abbandonato. C'è lo stesso clima che trovi all'interporto la domenica.")

![image](../../images/post/2021-01-24/Stanford2.jpeg "Solo i più fortunati sono solo closed temprorarily.")

L'unica reazione negativa a questo libro è stato scoprire che ogni volta che vado a fare la spesa passo davanti al liceo di James Franco. Adesso andare a comprare l'insalata ha assunto questa sfumatura inquietante in cui rivedo l'attore amputarsi il braccio in 127 ore. Menomale che esiste il food delivery.

Comunque il libro secondo me è super consigliato. Lo stile di Masneri è molto scorrevole e per nulla pesante. Consigliato per tutti quelli che sono un minimo curiosi di una realtà molto diversa da quella italiana, ma anche molto diversa da quella del resto dell'America. Se volete abbandonare i pregiudizi, questo è il libro che fa per voi.

Peace and love ❤️
