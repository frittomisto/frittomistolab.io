---
title:  "Storie di sirene e pirati"
date:  2021-05-23T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-05-23/2021-05-23-Fable.png"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
  - "siren"
---

🇮🇹️ Quando ero piccola *La Sirenetta* era uno dei miei cartoni Disney preferiti. L'ho visto e rivisto talmente tante volte che potrei probabilmente doppiarlo mentre dormo. Crescendo, *Black Sails* è una delle serie tv più belle che io abbia visto. Nonostante questo i libri su sirene e pirati non sono generalmente il mio genere. Non me ne vogliano male i fan ma aborro tutti il filone Joules Verne, Robert Stevenson e compagnia varia. Non ce la posso fare, è più forte di me. L'incipit di Moby Dick lo sopporto solo se lo sta leggendo Mara Wilson nel film Matilda.

<!--more-->

Ultimamente però volevo riconsiderare questo astio generale perchè ci sono state tante uscite interessanti che volevo leggere. L'unica cosa che continuava a frenarmi è che io di termini navali ne so meno di zero in italiano (nel senso che sonosco solo poppa e prua e neanche so quale è il davanti e quali il dietro), figuriamoci in inglese. Alla fine mi sono convinta pensando che una volta capito che i pirati facevano cose, negli interessi della trama poco importa poi effettivamente cosa stessero facendo e se lo stessero facendo a poppa o a prua. 

Oggi ho raccolto qui un recap delle ultime letture su questo genere. L'unico filo conduttore generale è che le figure genitoriali di tutti i romanzi avrebbero bisogno di una terapia psicologica intensiva su come fare i genitori. Non chiedo i genitori perfetti ma qualcuno che non picchi, abbandoni o maltratti i figli mi sembra il minimo.


### 1. To Kill a Kingdom
![image](../../images/post/2021-05-23/2021-05-23-KillAKingdom.png "A destra la versione italiana che uscirà per Fanucci editore il 27 maggio. Il perchè la sirena abbia una stella marina sulla faccia sfugge la mia comprensione.")

To Kill a Kindom, che in Italia arriverà fra poco tradotto come *La regina delle sirene* (?!), è quello che tra tutti probabilmente più si avvicina alla storia classica della Sirenetta. Lira è la figlia della regina delle sirene e come tale destinata a ereditare il trono. Ogni anno, per il loro compleanno, le sirene affiorano in superficie e con la loro voce incantano principi di vari regni per rubargli il cuore. Letteralmente, non in senso figurato. Più cuori una sirena colleziona e più il suo status è importante. Capite quindi che i rapporti tra il mondo delle sirene e quello dei mortali non è decisamente idilliaco. La guerra tra i due regni va avanti da anni, le due specie si uccidono a vicenda e tra tutti il principe Elian è quello che ha ucciso più sirene nel tempo. Se Lira è considerata il flagello degli umani per la sua brutalità, Elian è considerato il flagello delle sirene. Tutto cambia quando un giorno Lira commette un errore che la madre (alias tiranna sanguinaria) non le perdona. Come punizione Lira perde la sua magia e viene trasformata in mortale. L'unico modo per tornare sirena è quello di consegnare alla madre il cuore di Elian. Peccato che se Elian scoprisse chi è veramente la ucciderebbe all'istante.
	
Rispetto alla storia classica, la versione di Alexandra Christo ha un twist decisamente più dark, meno incentrato sul romance tra i due protagonisti ma sulla guerra tra le due specie. Vero fulcro del romanzo è la crescita personale dei due protagonisti e il peso che il passato comporta. Chi ha passato tutta la vita ad essere un assassino può smettere di esserlo? Se due specie non hanno fatto altro che uccidersi vicendevolmente è possibile trovare la pace o l'unica soluzione è lo sterminio di una delle due? Ma seppure se per salvare la tua gente, il genocidio è davvero la scelta giusta o solo la più facile?
	
Onestamente, a me è piaciuto.

### 2. Fable e Namesake
	
![image](../../images/post/2021-05-23/2021-05-23-Fable.png "Un minuto di silenzio per le copertine.")

In questo caso niente sirene ma solo pirati che però sono più mercenari su una barca che pirati veri e propri. Fable è la figlia di uno dei pirati più potenti del regno e dopo un naufragio che ha ucciso la madre, viene abbandonata dal padre su un'isola di ladri e senza cibo. Se Fable sarà in grado di sopravvivere e di trovare un passaggio verso il padre allora lui la riprenderà al suo fianco. In un mondo guidato dal profitto, Fable deve evitare i ladri dell'isola e cercare gemme sul fondo del mare per guadagnarsi un passaggio. Nessuno sa chi o che cosa sia e nessuno deve mai scoprirlo affinchè Fable abbia una chance di sopravvivenza. Quattro anni dopo, Fable riesce a pagare il suo passaggio al giovane West, un pirata e commerciante con cui la giovane ha spesso fatto affari negli anni. Una volta raggiunta la terraferma però, niente è come Fable si aspettava. Il sogno di potersi ricongiungere con il padre è solo un sogno e le rivalità e i complotti di poteri tra i mercenari la incastreranno in una rete da cui sarà impossibile liberarsi. Quando anche West si rivelerà essere solo una pedina nella mani del padre, Fable sarà costretta a venire a patti con chi è e cosa può fare per sopravvivere.
	
Adrienne Young secondo me scrive molto bene ed è ben capace di rappresentare i conflitti interiori e le battaglie dei protagonisti. Proprio per questa componente intrespottiva però, i libri peccano ogni tanto di un ritmo che ho trovato eccessivamente lento. Avrei apprezzato in alcuni punti una Fable meno alla deriva degli eventi e con una volontà più forte. Poprio per questo rispetto a *Sky in the Deep*, altro libro della scrittrice che invece ho amato, ho trovato la duologia meno incisiva e quindi non mi ha convinta al 100%.
	
### 3. Daughter of the Pirate King e Daughter of the Siren Queen
	
![image](../../images/post/2021-05-23/2021-05-23-Daughter.png)

Il capitano Alosa è la figlia del re dei pirati, personalmente addestrata del re con metodi decisamente non montessoriani viene mandata in una missione segreta per recuperare un pezzo di una mappa antica. La missione consiste nel farsi rapire e imprigionare da una nave nemica in modo che la prigionia le fornisca la scusa perfetta per essere sulla barca e cercare la mappa (chi di voi non sta leggendo questa recensione pensando alla pubblicità del Montenegro? *l'antico vaso andava portato in salvo*...). L'unico ostacolo tra Alosa e la mappa è una ciurma di pirati non eccessivamente friendly e il fratello del capitano, Riden, convinto che Alosa stia nascondendo qualcosa. La prigioneria, tuttavia, ha più di un asso nella manica e la finta prigionia non è l'unico segreto che custodisce. Il re dei pirati non è però l'unico alla ricerca della mappa e recuperare il pezzo mancante sarà più difficile del previsto per Alosa. Quella che doveva essere un'ordinaria missione si rivela essere un viaggio alla scoperta di chi veramente Alosa decide di essere e di chi si può fidare.
	
Onestamente ho comprato la duologia perchè per sbaglio ho cliccato compra sull'app del Kindle. Letteralmente i libri sono finiti nella mia libreria per sbaglio e neanche mi ispiravano molto. Che bello quando il mio intuito è totalmente sbagliato. Ho decisamente apprezzato la storia di Alosa e della sua ciurma ed è stata una lettura estremamente piacevole. Sicuramente come young adult rientra più nella fascia young che adult, ma se cercate una lettura leggera a tema pirati e sirene allora potrebbe fare al caso vostro. Alosa è un personaggio secondo me perfettamente riuscito, ironica e determinata nella prima parte e tormentata ma comunque determinata nella seconda. Non dico la lettura della vita, ma simpatico.
	
	
### 4. Roaring
![image](../../images/post/2021-05-23/2021-05-23-Roaring.jpg)

In questo caso entriamo totalmente in un altro genere di sirene. Ambientato nell'America degli anni venti, Erin lavora in un bar in cui occasionalmente si esibisce come cantante. A parte le sue esibizioni sul palco, però, Erin non parla mai con nessuno. Scappata da un passato come prigioneria che non ricorda, chi l'ha portata in salvo l'ha messa in guardia sul potere della sua voce e la spinge a non parlare. Quando usa la sua voce, infatti, Erin inavvertitamente spinge le persone a fare quello che vuole con conseguenze solitamente non piacevoli. Una sera però, per evitare una sparatoria nel bar, una singola parola rivela alla persona sbagliato chi è veramente, l'ultima sirena rimasta. Una volta scoperta dal governo, Cole è l'agente speciale mandato in missione a recuperare, imprigionare e riportare al Bureau il mostro Erin. Perchè con un potere come quello che possiede, la sirena è considerata un mostro e come tale una minaccia per tutti. Quando però Cole trova Erin quello che ha davanti non è un mostro ma una ragazza il cui scopo è esattamente l'opposto di quello di far del male agli altri. Peccato che il governo non è l'unico interessato alla ragazza e i due protagonisti si ritrovano a scappare da mobster, agenti e mostri come minotauri, vampiri e mutaforma. Niente e nessuno è quello che sembra e tutti sono interessati ad avere per se il potere della sirena.
	
Il mix tra una realtà di mobster e agenti del governo e quello fatto da creature fantastiche funziona incredibilmente bene in questo libro. Le atmosfere degli anni venti permeano le pagine del libro e danno un'aria vintage alla storia di Erin e Cole. Il romance è probabilmente la parte centrale del libro e onestamente la cosa che mi ha convinto meno tra tutte. L'idea di esplorare cosa effettivamente rende un mostro un essere mostroso (vale di più avere dei poteri o come decidiamo di usarli?) non è eccessivamente originale ma comunque apprezzata. Tutto sommato una storia fantasy romance piacevole da leggera e con un'aria un po' vintage.

Peace and love ❤️
