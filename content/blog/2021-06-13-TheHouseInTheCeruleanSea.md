---
title:  "The House in the Cerulean Sea"
date:  2021-06-13T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-06-13-TheHouseInTheCeruleanSea.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
---      

🇮🇹 Qualsiasi cosa voi stiate facendo, qualsiasi genere letterario voi prediligiate, in qualsiasi stato mentale voi siate:

> **abbandonate tutto e leggete questo libro.**

<!--more-->

L’imperativo categorico di Kant nella sua prima formulazione diceva qualcosa tipo “*agisci soltanto secondo quella massima che, al tempo stesso, puoi volere che divenga una legge universale*”. 
Bene, si prenda nota che tale massima nell’anno 2021 è stata da me definita come **leggere _The House in the Cerulean Sea_** di T. J. Klune.  
Complessi di onnipotenza ne abbiamo? Probabilmente sì, non giudicatemi, ché non è carino.


Credo onestamente che il modo migliore per descrivere questo libro sia attraverso un’immagine visiva. È una domenica di inverno (quindi non il miglior giorno della settimana perché vuol dire che domani è lunedì), fuori nevica, siete a casa da soli perché pandemia e isolamento sono ancora ben lontani dall’essere finiti.
Per combattere il freddo e la depressione accendete il camino, vi avvolgete in una coperta e bevete una cioccolata calda davanti al fuoco. 

Ora, magari a giugno questa immagine vi fa solo sudare, ma l’idea è chiara no? Questo libro è un concentrato di tutte queste tre cose: coperta, camino e cioccolata. È una coccola per l’anima come poche mi è capitato di leggere nella mia vita. È un libro che, se ridotto alla trama che racconta, non riesce ad esprimere i sentimenti positivi che trasmette, quindi non fatevi ingannare e non fermatevi allo strato superficiale della storia che racconta.

> “We should always make time for the things we like. If we don't, we might forget how to be happy.” 

Linus Baker è il nostro protagonista e vive una vita che, ad essere gentili, potremmo definire solitaria e tranquilla; ad essere brutali, potremmo classificarla come triste. Linus vive per il lavoro, a cui dedica anima e corpo seguendo il regolamento che rilegge più spesso di quanto Papa Francesco legga la bibbia. Come \*case worker\* per il \*Department in Charge of Magical Youth\*, Linus è una specie di sovrintendente incaricato di giudicare le condizioni e il benessere di bambini magici ospitati in orfanotrofi di proprietà del dipartimento. In caso di giudizio negativo, l’orfanotrofio viene chiuso e i bambini riassegnati. Sebbene il suo lavoro potrebbe risultate antipatico a molti, Linus è tutto sommato un cuore d’oro, vuole solo il benessere dei bambini e, sebbene il regolamento gli impedisca di legarsi a loro, si percepisce la sua genuina preoccupazione. Un giorno, in maniera del tutto inaspettata, Linus viene convocato ai piani alti dell’ufficio e gli viene assegnato un incarico di massima segretezza. Per un mese dovrà sovrintendere l’orfanotrofio nell’isola di Marsyas e mandare report settimanali sulle condizioni dei bambini e sul direttore che li educa. I bambini mandati in questo orfanotrofio sono considerati pericolosi ed emarginati non tanto per le loro azioni, ma per quello che potenzialmente potrebbero fare. Condannati per essere quello che sono i sei bambini sono: un gnomo, un folletto, un blob non ben identificato, un mutaforma, una viverna e l’anticristo (inteso proprio come figlio del diavolo). Linus sarà costretto a mettere da parte le sue paure e ad esprimere un giudizio obiettivo sulla pericolosità o meno dei bambini e sul misterioso direttore Arthur Parnassus.

La storia di Linus, dei bambini dell’orfanotrofio di Marsyas e del suo direttore è una straordinaria storia di accettazione, di lotta contro i pregiudizi e la paura che spesso si trasforma in odio verso qualcosa che non conosciamo o che non capiamo. Da un lato vediamo Linus, la cui colpa maggiore è quella di non esprimersi, di non fare domande, di seguire le regole imposte senza domandarsi se siano giuste o sbagliate condannando se stesso ad una vita triste, vuota, senza legami o amore. Al lato opposto troviamo gli abitanti del villaggio vicino all’isola che, con il pretesto di potersi esprimere, trasmettono messaggi di odio, di razzismo e di emarginazione. Nel mezzo di questi poli opposti troviamo un’isola felice i cui abitanti sono dei bambini, speciali per quello che sono, ma comunque bambini con l’innocenza tipica che li contraddistingue. 

> “Regardless of what else he is, he is still a child, as they all are. And don’t all children deserve to be protected? To be loved and nurtured so that they may grow and shape the world to make it a better place? In that way, they are no different than any other child in the village, or beyond. But they’re told they are, by people such as yourselves, and people who govern them and our world. People who put rules and restrictions in place to keep them separated and isolated. I don’t know what it will take to change that, if anything. But it won’t start at the top. It’ll start with us.” 

*The House in the Cerulean Sea* è una storia di amore, di accettazione, di combattere per quello che è giusto, di non avere paura a cercare la felicità e un posto nel mondo da chiamare casa fatto da persone che amiamo e che ci amano a loro volta, nonostante i nostri difetti o quello che siamo. Onestamente non ho idea del perché’ stiate ancora leggendo questa recensione invece che andare a leggere il libro. A luglio verrà portato tradotto e portato in Italia da Oscar Vault. Non avete nessuna scusa.


Peace and love ❤️