---
title:  "Un'italiana in America"
date: 2021-01-03T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-03_usa_flag.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "Life style"
tags:
  - "italian review"
  - "American dream"
  - "Life style"
  - "USA"
---

🇮🇹️ Da quando mi sono trasferita in America mi vengono fatte quasi unicamente tre domande dai miei connazionali:
1. Quindi adesso anche tu mangi la **pizza con l'ananas**?
2. Come stanno gestendo la **pandemia** in America?
3. Come hanno fatto gli americani a votare **Trump**?

Vi avviso, non sarà questo il giorno e il luogo in cui riceverete risposte serie ed esaustive a queste domande. Io non ne capisco nulla di cucina, non ne so nulla su come sia il modo migliore per gestire un'epidemia e di politica ne capisco meno di zero. Cosa vi aspettate che vi risponda?

<!--more-->

In un anno come il 2020 che mi ha tolto qualsiasi certezza, sono molto felice di sapere che almeno una è rimasta. La pandemia in corso, le proteste sociali, l'elezione presidenziale, il cambiamento climatico e tutto quello che è successo questo anno, ma comunque il primo pensiero di chi mi circonda è il cibo. Sintomo che so scegliere perfettamente di chi circondarmi (o per nulla, dipende dai punti di vista). Allora parliamo di questa famosissima pizza con l'ananas, ogni tanto sostituita dagli spaghetti con il ketchup, ma solo dai miei amici più temerari. Long story short, no, non mangio pizza con l'ananas (neanche spaghetti con il ketchup nel caso siate curiosi). Ma non perchè io sia particolarmente snob con quello che arriva nel mio stomaco. Anzi, considerando che io e la cucina siamo due rette parallele destinate a non incontrarsi mai, probabilmente la pizza con l'ananas sarebbe un risvolto in positivo rispetto ad un mio pasto medio. Io sarei anche curiosa di provarla, lo ammetto. Il problema (anche se per molti sarà motivo di felicità) è che non è così tanto facile da trovare. A parte se vai a prendere la pizza da Domino, ma se sei tra quelle persone non possiamo essere amici, sorry. Comunque, no, non è facile da trovare. Perchè? Per il semplice motivo che la maggior parte delle pizzerie che frequentano gli italiani in America (me compresa perchè chi sono io per distinguermi dalla massa?) sono di italiani o di figli di immigrati italiani o comunque di persone con un parente prossimo italiano (anche una zia da parte di padre conta). Quindi tutte persone a cui se chiedi l'ananas sulla pizza ti rispondono parole molto colorite in almeno due lingue diverse e ti accompagnano alla porta (dopo averti chiesto la mancia perchè ormai l'acqua te l'hanno versata). Perchè, bisogna ammetterlo, noi italiani siamo abbastanza rompicoglioni con il cibo. Possiamo anche emigrare oltreoceano rinnegando la madre patria, ma comunque siamo cresciuti con l'assoluta convinzione che sul cibo siamo avanti anni luce e che quindi no, noi agli hamburger non ci piegheremo e mangeremo solo pizza a Little Italy. Salvo che poi invece alla fine ci pieghiamo eccome. Viva gli hamburger. Viva il junk food. Hai detto mac and cheese per cena?

Dopo la domanda di rito sulle mia abitudini alimentari, le persone solitamente ritrovano la lucidità mentale e si ricordano che c'è una pandemia in corso e vogliono sapere come gli americani la stiano gestendo. Ammetto che non so mai bene come rispondere a questa domanda. No, veramente, come? Cioè, anche se guardi i grafici dei contagi esattamente come io guardo la cartina di google maps (ovvero senza speranza di capirci mai niente) basta vedere che la linea che rappresenta l'America è sopra tutti gli altri. Numero di morti, casi giornalieri, casi totali, media delle ultime settimana, rapportate alla popolazione, qualsiasi cosa, gli americani comunque sono avanti. E questo è sicuramente uno dei pochi casi in cui essere avanti non è cool. Quindi, evidentemente, non la stanno gestendo bene. La domanda giusta sarebbe probabilmente più sul perchè gli americani non hanno saputo gestire bene la pandemia. Ma una risposta seria ed esaustiva supera le mie conoscenze (non è che vivendo in America uno diventa automaticamente il massimo esperto) e, sconvolgerò molti con questa affermazione, ma non è un crimine ammettere le proprie lacune. In Italia, durante il primo lockdown, ho visto un'intervista in un programma TV in cui veniva chiesto a Luca Argentero di commentare la pandemia perchè in una serie aveva interpretato un dottore. Ecco no, per carità, qualcuno me ne scampi. Certo, chiunque può dare il suo parere su qualsiasi cosa, ma fino ad un certo punto.


![image](../../images/post/2021-01-03_zeroc.jpg "lollliofsdfgkjfsdlkfj")
<!--<figure>
  <img src="../../images/post/2021-01-03_zeroc.jpg"/>
  <figcaption> LOL </figcaption>
</figure>
Via Facebook @[(Z)ZeroCalcare](https://www.facebook.com/profile.php?id=100044154743385) -->

Io, per esempio, posso provare a descrivere la gestione della pandemia della mia contea. Ma, udite udite, il quarto segreto di Fatima era che l'America è un paese enorme. Incredibile vero? Ok, tutti sanno che l'America è un paese enorme, ma in realtà è proprio ENORME. Alcune città sono grandi quanto intere regioni italiane. Quindi effettivamente poi ogni stato gestisce le cose a modo suo, bene o male che sia, ma quello che vi raccontano dal Wyoming non è la stessa storia made in New York. E il governo federale? Penso che si sia perso lungo il tragitto del beviamo il disinfettante e facciamoci delle lampade per combattere il virus. Non pervenuto. Ripassate il 21 gennaio.

La domanda sulla pandemia, come potete ben capire, è accompagnata quasi sempre a braccetto della domanda sulla politica. Ma perchè Trump ha gestito le cose in questo modo. Ma come hanno fatto gli americani a votarlo. Ma come è possibile che nessuno gli dica nulla. Allora, siatemi essere ripetitiva in questo post. Io di politica ne capisco meno di zero. Meno di google maps che è tutto dire visto che riesco a perdermi dentro il mio monolocale. Ne capivo poco in Italia e non è che vivendo in America divento automaticamente esperta in materia. Capite quindi che le mie conversazioni ultimamente sono abbastanza deprimenti. Per fortuna esiste la possibilità di una connessione internet scadente per salvarsi.

>"No guarda non ti sento più, sto entrando in galleria, non prende più il 4G."
"Ma non eri a casa?"
"......"

Un sassolino dalla scarpa però me lo vorrei togliere. Ma noi italiani possiamo veramente permetterci di giudicare a cuore così leggero le scelte politiche degli altri paesi? Per carità, gli americani nel 2016 hanno votato Trump, ma almeno venivano da otto anni di Obama. Il nostro Obama italiano a livello di spessore morale chi è stato? Io non saprei rispondere a questa domanda. Comunque, parentesi seria, le ragioni che hanno portato Trump alla guida del paese libero ci sono, sono tante e sono complesse. I giornalisti seri ci scrivono articoli, libri e servizi a riguardo quindi ben lungi da me rispondere a questa domanda. Se siete veramente curiosi cercate Francesco Costa su Instagram _et voilà, les jeux sont faits_.

Non volevo concludere questo flusso di coscienza con una parabola discendente nella serietà che poco mi si addice. Quindi chiudiamo con un aneddoto che solitamente mi salva se la conversazione con il mio interlocutore ha preso una piega troppo seria. La pasta con il ketchup è probabilmente una delle tante esagerazioni che dall'America arrivano all'Italia. Tipo quando giochi al telefono senza fili iniziando con gatto e l'ultima persona della catena grida lapalissiano (wtf?). Io onestamente un americano che abbia mai pensato di mangiare la pasta con il ketchup lo devo ancora incontrare. Tuttavia, ne ho conosciuto più di uno convinto che i tempi di cottura della pasta non siano dati dal numerino magico riportato sulla confezione ma dalla capacità della pasta stessa di rimanere attaccata al muro se la lanci. Se invitate americani a cena suggerisco la pizza da asporto piuttosto che un piatto di pasta. Con l'ananas o no, fate voi.

Peace and love ❤️
