---
title:  "La Stirpe della Gru"
date: 2021-01-20T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-20_gru.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
  - "asian culture"
---  

🇮🇹️ *La Stirpe della Gru* è il debutto young adult di Joan He e fa parte del filone Chinese-inspired fantasy che ultimamente in maniera del tutto casuale occupa a pieno il mio tempo di lettura. In Italia è uscito il 19 Gennaio per Oscar Vault, ergo siamo qui (ma siamo chi poi? io e le mie multiple personalità?) per fornirvi le mie personalissime e assolutamente non richieste opinioni a riguardo.

<!--more-->

Dopo aver letto questo libro ho dovuto onestamente prendermi un momento di riflessione per capire. Ma mi era piaciuto o no? Bho, non avrei saputo dirlo. Di solito arrivati alla fine di un libro abbiamo ben chiaro se il libro in questione ci è piaciuto o no, magari dobbiamo riflettere sul perchè e il per come, ma se un libro per voi è sì o no una volta arrivati alla fine dovreste saper dare una risposta. Con La Stirpe della Gru a me non è successo, è stato un viaggio sulle montagne russe delle emozioni e non è stato immediato capire se alla fine aveva prevalso il sì o il no. Ma partiamo dalla trama.

Nel regno di Yan il re muore in maniera improvvisa e la figlia primogenita, la principessa Hesina, è costretta a prendere in mano il potere e diventare regina. Hesina amava intensamente il padre, unica figura della sua famiglia con cui aveva un legame stretto se consideriamo che con il fratello a malapena intrattiene dei rapporti cordiali e la madre la detesta e fa l'eremita sulle montagne. Sconvolta quindi dalla sua perdita e dopo aver assistito a strani fenomeni sul corpo del genitore morente, Hesina è determinata a far luce e trovare la verità sulla morte del padre. Per farlo, si spinge addirittura a compiere quello che viene considerato un atto di tradimento punibile con la morte: si rivolge ad un' indovina. Gli indovini sono esseri dotati di magia che più di seicento anni prima sono stati quasi totalmente sterminati durante la rivoluzione operata dagli Undici. Un tempo, infatti, gli antichi imperatori erano essere assetati di potere e controllavano il regno opprimendo i più deboli anche grazie all'aiuto della magia degli indovini che prevededevano il futuro. Gli Undici furono dei banditi che posero fine al regno di oppressione degli imperatori, liberarono gli schiavi, aprirono le porte dell'istruzione a chiunque, donne e meno abbienti compresi, e bandirono la magia e gli indovini dal regno. Praticamente una piccola rivoluzione francese versione magia brutta e uguaglianza bella (anche se uguaglianza è opinabile perchè non credo che gli indovini si sentissero trattati in maniera molto egualitaria:*Gli Unidici hanno liberato gli oppressi opprimendo gli oppressori*). Comunque, Hesina è determinata a conoscere la verità e l'indovina le svela che per scoprirla dovrà rivolgersi ad un fuorilegge dotato di un asta (nessun doppio senso, maliziosi) e imprigionato nelle sue segrete. Hesina quindi chiede un formale processo per trovare l'assassinio del padre e stringe un patto con Akira, il delinquente in prigione, per farsi rappresentare in tribunale. Il processo metterà in moto una serie di eventi che porteranno il regno sull'orlo della distruzione e Hesina a chiedersi quanto sia disposta a pagare per la verità. Soprattutto, possiamo amare qualcuno che scopriamo di non conoscere? Possiamo sacrificare tutto quanto in nome di questo amore? E fino a quando il fine giustifica i mezzi?

>  Cos’è la verità? Gli eruditi la cercano. I poeti la scrivono.
I sovrani benevoli pagano oro per udirla. Ma in tempi difficili, la verità è la prima cosa che tradiamo.

Come dicevo all'inizio per me questo libro non è ne un sì ne un no. Non è scritto male, non ha grossissimi difetti che non me lo facciano piacere, ma non ha nemmeno nulla me lo abbia fatto amare particolarmente. Ho fatto una gran fatica a leggere le prime 200 pagine perchè continuavo a distrarmi e a preferire altro. Non un buon segno no? Successivamente il ritmo si fa più incalzante, gli eventi politici prendono il sopravvento e la protagonista comincia a fare le scoperte più sconcertanti per lei. Però è tutto abbastanza prevedibile, quelli che dovrebbero essere colpi di scena non lo sono veramente e il lettore si aspetta praticamente tutto quello che succede. So, what's the point?

La protagonista è un personaggio abbastanza ben definito, tutte le sue decisioni sono basate sul dolore di aver appena perso un padre che amava e la vediamo sgretolarsi pian piano quando si rende conto che il genitore non è chi credeva di conoscere. Considerando che per lei è un fattore così determinante, avrei quindi preferito vedere all'inizio della storia qualcosa di più sul rapporto padre-figlia. Invece il romanzo parte che il padre è già morto ed è quindi un personaggio che riusciamo a conoscere solo di riflesso. In più, non ho apprezzato le insicurezze caratteriali di Hesina. Questa ragazza nasce e viene cresciuta per essere poi un futuro la regina, perchè mai dovrebbe credere così poco in se stessa? Ha avuto un padre che la amava immensamente e che le ha insegnato tutto, perchè una volta effettivamente diventata regina non dovrebbe essere pronta ad esercitare il suo potere? Capisco nel momento in cui perde la fiducia verso il padre, ma all'inizio ha poco senso secondo me. Non è che tutte le donzelle al potere devono avere per forza la stessa autostima di una nutria. Nel finale, poi, il personaggio di Hesina viene secondo me completamente rovinato. Per tutto il libro cerca di contenere i danni delle sue scelte per proteggere il popolo verso cui si sente responsabile, anche se non lo comprende appieno, e poi che fa? Boom, buttiamo tutto a mare e fanculo tutti con la sua scelta finale. Ma come? Se sei arrivata fino a qui non ha senso abbandonare la lotta ora.

Il personaggio maschile di Akira è senza ombra di dubbio il personaggio peggiore. Nel senso che fa poco o nulla, ha un ruolo chiave in un paio di occasioni, ma poteva benissimo essere sostituito da un altro e la storia sussisteva tranquillamente senza di lui. Non un destino felice per il personaggio maschile principale. Per di più, il romance nel romanzo non ha nessun senso di esistere. C'è una protagonista femminile, uno maschile, quindi perchè non farli baciare ad un certo punto? Ma, caro scrittore, non è mica obbligatorio eh. L'ho trovata una forzatura non necessaria e assolutamente inutile ai fini della storia. Di certo il mio animo romantico non viene soddisfatto da un sentimento che nasce senza nessuna base, i due neanche arrivano a conoscersi veramente.

Molto più preponderante è il personaggio del fratello adottivo di Hesina, Caiyan, che però lo si riesce ad inquadrare nel primo capitolo del romanzo. Quindi, interessante sì, ma il lettore si aspetta già tutta la sua evoluzione quindi i colpi di scena suscitano solo sensazioni tipo "sì ok va bene, andiamo avanti, lo avevamo capito 250 pagine fa che finiva così".

Tutti questi elementi fanno sì che per me alla fine sia stato un 3/5 perchè non è un brutto libro, ma l'emozione finale che ha vinto è stata più che altro la noia e non ho la minima curiosità nel sapere la storia come continua nel seguito. Se volete buttarvi nel filone Chinese-inspired fantasy io onestamente vi consiglierei *Spin the Dawn* o *These Violent Delights* prima di leggere questo.

Peace and love ❤️
