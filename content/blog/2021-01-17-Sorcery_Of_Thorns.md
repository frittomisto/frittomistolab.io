---
title:  "Sorcery of Thorns"
date: 2021-01-17T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-01-17_sorc.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "fantasy"
  - "young adult"
---  

🇮🇹️ *Sorcery of Thorns* è il secondo romanzo di Margaret Rogerson, ma per quanto mi riguarda il mio primo approccio alla scrittrice (poi ho recuperato anche il primo, tranquilli). In Italia uscirà il 19 Gennaio per Oscar Vault, quindi quale migliore occasione per darvi il mio personalissimo e non richiesto parere a riguardo.

<!--more-->

Se esistesse una religione dei lettori, il primo comandamento sarebbe quasi sicuramente qualcosa del tipo *leggi qualsiasi cosa includa una biblioteca magica, dei libri viventi o affini*. L'equivalente per gli scrittore dovrebbe essere *se vuoi includere questi argomenti, non scrivere una merda, per favore*. Scusate il francesismo. Con queste premesse, parliamo del libro.

La protagonista di Sorcery of Thorns è Elisabeth Scrivener che da bambina viene abbandonata davanti alla Grande Biblioteca di Summershall e lì viene cresciuta. La biblioteca è abitata da guardiani dei libri (nella mia mente monaci bibliotecari ma con le spade), dalla direttrice della biblioteca e ovviamente dai libri. Questi ultimi, però, nel mondo di Elisabeth non sono libri comuni ma oggetti animati, grimoire magici, dotati di una personalità propria dettata dal loro contenuto. In quanto tali, libri con un contenuto pericoloso (tradotto in incantesimi di vario tipo) vengono tenuti sotto chiave e controllati dai guardiani. Se provocati, infatti, i grimoire particolarmente potenti sono in grado di trasformarsi in mostri enormi di pagine e inchiostro. Una sera Elisabeth viene svegliata nel sonno e scopre la direttrice della biblioteca uccisa da una di questi mostri. Accusata di aver liberato il libro responsabile dell'omicidio, Elisabeth viene prelevata da uno stregone, Nathaniel Thorn, per essere portata in città e interrogata. Lo stregone viene visto con una certa riluttanza dalla ragazza in quanto la magia degli stregoni discende unicamente da un loro patto con un demone: magia in cambio di anni di vita. Il demone Silas che accompagna Nathaniel è tuttavia quasi un demone valletto, un tuttofare che non sembra avere assolutamente nulla di demoniaco. Il trio dei personaggi arrivato in città resterà immischiato in un complotto centenario con lo scopo di distruggere tutte le Grandi Biblioteche e il mondo insieme a loro. Per salvare entrambi, Elisabeth dovrà mettere in discussione tutto quello che i guardiani dei libri le hanno insegnato, dovrà imparare a fidarsi dello stregone Nathaniel e sarà costretta a lavorare con il demone Silas.

> She now understood that the world wasn't kind to young women, especially when they behaved in ways men didn't like, and spoke thruths that men weren't ready to hear.

Questa è la trama a grandi linee. Prima domanda classica, ti è piaciuto il libro? No. Chiaro e tondo, non voglio mancare di rispetto a nessuno, ma per me è un enorme non ci siamo. L'idea del mondo in cui ci sono biblioteche con libri dotati di una personalità è una bella idea e in generale il worldbuilding è la cosa meno peggio del romanzo. C'è anche un accenno di intreccio tra il mondo della magia degli stregoni e il progresso tecnologico che sembra interessante, ma peccato che non viene approfondito mai. Il vero problema di questo romanzo sono i personaggi. Non ce ne è uno che si salvi. Elisabeth è quella che viene descritta meglio, viene dotata di un accenno di personalità e viene superficialmente affrontato il suo dibattito interiore tra salvare il mondo e abbandonare tutto quello che le hanno insegnato crescendo per farlo. Solo superficialmente però, sia mai. In compenso viene ripetuto fino allo sfinimento il fatto che è una ragazza alta. Viene ripetuto talmente tante volte che ad un certo punto il lettore pensa che sia un dettaglio utile a qualcosa. Che senso avrebbe se no scrivermi 20 volte che è alta? Ma no, non serve a nulla, probabilmente bisognava occupare spazio e scrivere qualcosa sul carattere era evidentemente troppo difficile. Oltretutto, Elisabeth come personaggio è totalmente incostitente. Elisabeth cresce tutta la sua vita in una biblioteca in mezzo ai libri, vorrebbe diventare guardiana, ma ancora non ha iniziato l'addestramento per farlo quindi per ora è una semplice bibliotecaria. Una bibliotecaria che parla con i libri ok, ma sempre una bibliotecaria. Bene, la notte che trova la direttrice morta, Elisabeth le ruba la spada, affronta il grimoire diventato mostro e lo sconfigge. MA COME?? Fino a due secondi prima maneggiavi al massimo i segnalibri e in un secondo impari a maneggiare una spada e uccidi un mostro descritto come indistrittubile? Va bene il fantasy però la coerenza dai.

Sul personaggio di Nathaniel vorrei poter dire qualcosa a rigurdo, ma viene trattato solo superficialmente. Nathaniel è un personaggio senza spessore, orfano rimasto solo viene cresciuto dal demone Silas che diventa quindi la sua fata madrina. Quasi letteralmente eh, Silas gli rifà il letto, gli sistema la cravatta, gli prepara da mangiare, gli lava la biancheria. Posso avere un Silas anche io per le faccende domestiche? I due si fanno trascinare da Elisabeth in questa crociata per salvare il mondo, ma nessuno dei due in realtà vorrebbe averci nulla a che fare e non si sanno bene le ragioni per cui dovrebbero accettare. Comunque lo fanno perchè se no il libro finiva a pagina dieci. La cosa più bella di tutto, però, è che i due protagonisti, Nathaniel ed Elisabeth, sono in realtà assolutamente inutili alla fine della trama. Non servono a nulla, non risolvono nulla e fanno solo casini. Il loro viaggio per salvare il mondo è una continua tappa in cui arrivano troppo tardi e nel fortuito caso in cui riescano ad arrivare in tempo allora fanno solo più casino. Gli viene detto che se andranno in un posto e faranno una certa cosa allora finirà il mondo. Quindi cosa fanno? Vanno esattamente in quel posto e fanno esattamente quello. Poi si stupiscono oddio sta finendo il mondo. Ma in che senso scusa? Cosa non era chiaro? Menomale che c'è il tuttofare Silas che risolve tutti i loro casini, fossi stato in lui li avrei abbandonati molto prima, ma anche in questo caso il romanzo sarebbe finito a pagina undici. Oltretutto una relazione amorosa tra Silas e Nathaniel sarebbe stata l'unica cosa con un senso del libro, ma anche qui illusa io.

Nel romanzo c'è anche la figura di un cattivo che però è qualcosa di agghiacciante. Prima di tutto non si sa bene perchè stia provocando la fine del mondo. Ma il punto non è che il lettore non lo capisce, ma il personaggio stesso non lo sa. Lo fa perchè così la trama può andare avanti, altrimenti il romanzo finiva a pagina dodici (avete intuito il life motive del romanzo?). In compenso però a pagina tredici il cattivo ci spiega per filo e per segno tutti i suoi piani, probabilemnte lo fa con Elisabeth davanti sperando che così la poverina possa fare qualcosa, ma la scrittrice si è dimanticata di dirgli che fa tutto Silas non lei. Comunque il lettore a un terzo del libro sa già tutto. Che senso ha continuare quindi? Ci saranno dei colpi di scena? No, nessuno, il perseverare nella lettura rientra solo nella sfera del masochismo.

L'unica cosa che la scrittrice aveva pensato bene erano i libri animati che si trasformano in mostri. Verrà sfruttata questà capacità ai fini della trama? Illusi. I libri vengono effettivamente fatti combattere, ma quando lo fanno diventano tutti dei *Libri dei mostri* di Harry Potter. Ma perchè? Ma se possono trasformarsi in enormi mostri perchè tornare indietro nella scala evolutiva a libri che si aprono e chiudo e saltellano di qua e di la? Non me lo so spiegare, giuro.

Però alla fine secondo me Sorcery of Thornes si merita 2/5. Perchè così tanto? Perchè dopo questo libro, spinta evidentemente da un desiderio autodistruttivo, ho letto il primo romanzo della scrittrice *An Enchantment of Ravens* che si è dimostrato anche peggiore di questo (vi lascio immaginare). Se quindi An Enchantment of Ravens si è meritato 1/5, allora Sorcery of Thornes si merita 2. Apprezziamo il miglioramento, non si dica che sono ingrata.

Peace and love ❤️
