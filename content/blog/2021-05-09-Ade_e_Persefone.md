---
title:  "Le rivisitazioni del mito di Ade e Persefone"
date:  2021-05-09T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-05-09/ATouchTrilogy.png"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "romance"
  - "retelling"
  - "mythology"
---

🇮🇹️ Adoro quando compro un libro su Amazon e a ruota poi mi vengono consigliate tutte le diverse sfumature di uno stesso argomento e mi ritrovo così a leggere per una settimana di fila sei diversi libri sul mito di Ade e Persefone. Per alcuni potrà sembrare noioso, ma io adoro vedere come uno steggo argomento viene trattato in maniera differente. Ora però tutte le storie sono completamente intrecciate nella mia mente ed è con il post di oggi che voglio sbrogliarle e presentarvi le diverse sfumature di ognuno.

<!--more-->

Ovviamente essendo tutte rivisitazioni di un mito, ci sono tantissimi elementi in comune tra i libri: l'episodio del ratto di Persefone, le figure di Demetra e Ecate, il rapporto con Piritoo, la comparsa di Orfeo nell'oltretomba alla ricerca di Euridice e altri. Soprattutto, elemento comune tra tutte e tre le trasposizioni è l'idea di Ade come un dio solitario, emarginato dalle altre divinità e incompreso per la sua natura erroneamente percepita come malvagia. Ade è fondamentalmente un'anima in pena che non aspetta altro che la sua dolce metà compaia e lo salvi da un'esistenza solitaria. Persefone viene dipinta come una dea giovane, quasi più umana che divina e per questo spesso emarginata a sua volta. Con l'aiuto di Ade Persefone capisce il suo potenziale e i suoi poteri ed è in grado di sbocciare come la dea della primavera e regina dell'Oltretomba.

### 1. Il giudizio di Persefone

![image](../../images/post/2021-05-09/GiudizioPersefone.jpg "Finalmente un libro in italiano!")


 Di questo libro ho già parlato [qui](https://frittomisto.gitlab.io/books/Libri_Calligola/), ma rinfreschiamoci la memoria. In questo mondo, le divinità si sono rivelate all'umanità e conducono tutto sommato un'esistenza pacifica. Persefone è una studentessa di giurisprudenza alla sapienza di Roma e vive la sua vita come una comuna mortale. Il potere della primavera è tutto sommato soltanto una specificazione di quello che è il potere della madre Demetra e come tale Persefone si sente inutile come divinità. Un giorno, in un comizio all'università, Persefone incontra Ade che finirà per offrirle uno stage nell'Oltretomba. Quale miglior luogo per una studentessa di giurisprudenza se non quello dove avviene il giudizio finale delle anime? Se all'inizio del libro Persefone è una dea quasi inutile, nell'oltretomba riesce a trovare una sua dimensione e a sfogare tutto il suo potenziale nei Campi Elisi.
L'unione tra Ade e Persefone è inizialmente ostacolata da Demetra che non vuole vedere la figlia appassire sotto terra, ma successivamente la dea dei raccolti si ricrede in fretta e arriva a fornire la sua benedizione alla coppia.

**Cosa ha di diverso rispetto agli altri**: in questo libro il personaggio di Ecate non compare mai. La storia della distruzione di Crono e il suo imprigionamente nel Tartaro è quasi marginale. Demetra è una madre abbastanza normale, preoccupata per la figlia, ma ben felice di vederla per la sua strada quando capisce che è felice e non in pericolo. Simpatica la presenza delle altre divinità, anche provenienti da altre religioni.

### 2. Hades trials trilogy

![image](../../images/post/2021-05-09/HadesTrials.png "Copertine tamarre ne abbiamo?")

In questa rivisitazione Persefone è una ragazza normale, barista di New York con un gran pollice verde e dei genitori con un gusto discutibile sulla scelta dei nomi per i figli. Un giorno il belloccio di turno che la molesta e stalkerizza al bar si rivela essere Zeus che la rapisce portandola nell'oltretomba. Qui Persefone è accolta da Ecate che non solo le rivela come tutta la mitologia greca è vera ma che lei è QUELLA Persefone, prima moglie di Ade e regina dell'Oltretomba. Per non si sa bene quale motivo (rivelato poi nel terzo libro), a Persefone, dopo quattro anni di matrimonio con Ade, è stata cancellata la memoria, privata dei suoi poteri e mandata sulla Terra per vivere una vita come umana. Zeus, per un perverso senso dell'umorismo volto a far soffrire il fratello Ade, la riporta nell'Oltretomba per partecipare (senza possibilità di ritirarsi o di replica) ad una serie di prove volte a scegliere una nuova moglie per Ade. Il nuovo obiettivo di Persefone sarà quindi quello di partecipare alle prove e perdere volutamente per poi poter tornare alla sua vita umana. Nel corse dei tre libri, tuttavia, per non morire prima di arrivare in fondo, Persefone è costretta a recuperare i suoi poteri, far luce sul perchè è stata cacciata sulla Terra anni prima e rivelare chi nell'ombra sta cercando di usare la sua presenza nell'Oltretomba a suo vantaggio.

**Cosa ha di diverso rispetto agli altri**:  questo tra tutti è sicuramente il riadattamento con più azione, Persefone rischia di lasciarci le penne un libro si e l'altro pure. La figura di Demetra in questo caso è non pervenuta, Persefone è una dea comparsa un giorno in una foresta e cresciuta da Dioniso. In generale, la trilogia è quella che si distacca di più dal mito originale, favorendo più la parte di azione, il mistero di chi sta usando Persefone e il romance tra i due protagonisti.


### 3. A touch of darkness trilogy

![image](../../images/post/2021-05-09/ATouchTrilogy.png "Il terzo libro della trilogia uscirà a fine mese.")

In questo riadattamento Persefone vive in incognito tra gli umani studiando giornalismo all'università e vivendo con l'amica Lexa. Per tutta la vita Persefone ha vissuto (e vive) sotto l'ala protettrice della madre Demetra che più che una madre sembra il secondino di una prigione. Persefone tuttavia è priva di poteri propri e quindi totalmente dipendente da quelli della madre che le permettono di mantenere attivo un incantesimo di smascheramento che nasconde la sua natura divina. In una serata in un locale con l'amica Alexa, Persefone si imbatte inconsapevolmente con Ade con cui stringe (di nuovo inconsapevolmente) un contratto. Persefone adesso è costretta a creare vita nell'Oltretomba per tenere fede ai termi del contratto, peccato che Persefone non abbia nessun potere e le possibilità di far crescere qualche pianta con il metodo classico del pollice verde sono meno di zero. Il rapporto tra Ade e Persefone è un'altalena di alti e bassi, l'attrazione tra i due è sicuramente innegabile ma una serie di incomprensioni e segreti rendono il rapporto non proprio idilliaco. Nei primi due libri della serie (il terzo uscirà a fine mese), Persefone impara a sviluppare i suoi poteri e a emanciparsi dalla madre, impara che la morte è qualcosi di inevitabile e che non sempre è tutto bianco o nero.

**Cosa ha di diverso rispetto agli altri**:  Persefone sembra sviluppare poteri che hanno tutto il potenziale di renderla badass nel terzo volume della serie (spero). La madre Demetra è l'equivalente di un campo di grano a primavera quando soffri di allergia, bella da vedere ma potenzialmente letale.


Onestamente, tra tutti questi adattamenti mi sarebbe piaciuto vederne uno in cui Ade non è un bimbo abbattutto e Persefone non passa le sue giornate a sminuire se stessa. Avrei tanto voluto vedere una Persefone stile Rosario Dawson in Percy Jackson e gli dei dell'Olimpo. Perchè è così necessario dover scrivere di personaggi *buoni*? Per una volta vorrei una storia in cui lui è un fucking dio dell'Oltretomba e lei, sebbene dea della primavera, ha un certo twist dark che le permette di diventare la sua regina. Per ora rimane una storia nella mia testa, ma se è una storia che invece conoscete e avete letto, per favore condividete con il pubblico a casa (che in questo caso sono io).

Peace and love ❤️
