---
title:  "Rule of Wolves e l'universo Grishaverse"
date: 2021-04-25T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-04-25-RuleOfWolves.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "Leigh Bardugo"
  - "fantasy"
  - "young adult"
---

🇮🇹️ Long time, no see direbbero alcuni. Vorrei dirvi che in questo mese in cui non ho scritto nulla ho impiegato il mio tempo in attività incredibili. Ho lavorato tantissimo, ho salvato il mondo, trovato la cura per il cancro, esplorato la foresta amazzonica, fatto un rehab nel deserto. Se vi fa piacere immaginatemi così. Nella realtà dei fatti ho fatto esattamente la stessa vita che facevo due mesi fa, ho letto tantissimo, ma avevo zero voglia di parlare di quello che leggevo. Ovviamente giusto Leigh Bardugo poteva togliermi da questa impasse. So, here we are. Parliamo di Rule of Wolves, del Grishaverse e del mio ingiustificato amore per qualsiasi cosa che partorisce la scrittrice.

<!--more-->

Premetto che sono qui per parlare del libro, ma non per spingervi a leggerlo. Prendetelo più come un flusso di coscienza che come una recensione per capire se il libro fa per voi o no. Che poi, parliamoci chiaro. Fa per voi se avete letto i primi 6 libri del Grishaverse, altrimenti no. Easy peasy, lemon squeezy.  La premessa è per dirvi che ci saranno degli inevitabili spoiler che se volete evitare non andate oltre a leggere. Se volete una mia opinione su se leggere RoW o no, la mia risposta breve è si, fatelo. La mia risposta lunga lunga è si, fatelo, perchè non lo avete ancora fatto? Posso tranquillamente ammetter che la mia opnione è del tutto pesta dal fatto di essere una fangirl di Leigh Bardugo e del Grishaverse e come tale sono assolutamente e inevitabilmente di parte a prescindere. Abbiate pazienza, nessuno è perfetto.

Partiamo da Rule of Wolves, seguito di King of Scars e momentanea chiusura del Grishaverse. Momentanea perchè Leigh (mi prendo la libertà di chiamarla per nome come se fossimo bff, ma nella mia mente lo siamo, quindi va bene così) pare che per ora non abbia intenzione di continuare a scrivere su questo, ma la storia si presta benissimo a continuare in futuro. Io onestamente ero un po' scettica su RoW perchè King of Scars lo avevo apprezzato ma non amato. Non ci avevo trovato la genialità della duologia di Six of Crows però un libro su Nikolai, Zoya e Nina non si poteva non leggerlo. RoW continua sulla scia di KoS, ma aggiungendo cose. Tante cose. 600 pagine di cose. Tutte necessarie? Sorry, but no. Ad esempio, tutto il filone della ribellione dello Shu Han era necessario? Avrei potuto leggere RoW senza tutti i capitoli di Mayu e la storia sarebbe andata avanti e finita esattamente allo stesso modo senza problemi. Il che poteva forse spingere Leigh a intuire che c'era un problema. Altro esempio, Nikolai e Zoya tornano a Ketterdam per rubare dell'alluminio e costruire dei missili per la guerra. Bello, perchè ritroviamo Kaz, Jesper e i corvi (colpo basso per i fan di Six of Crows, Leigh), ma poi si rileva assolutamente inutile perchè l'alluminio non è abbastanza e i missili non si fanno. Stessa sorte tocca ai pochi ma assolutamenti inutili camei di Alina e Mal. Mi sembrava strano che non comparissero fino a metà di RoW, ma per quello che è stato il loro ruolo tanto valeva lasciarli dove stavano. Così la loro presenza mi è sembrato più come un tributo di Leigh, un modo per i fan di rivederli, salutarli e passare oltre.

Tanti altri filoni vengono portati avanti, ma senza poi effettivamente sfociare in qualcosa di significativo e importante. Nikolai viene accusato dai tempi della trilogia di Shadow and Bone di non essere il figlio del re e la storia sulla sua genealogia finisce con un "si è vero non lo sono, ma tanto non voglio più il trono". Ma come non vuoi più il trono? Hai combattuto e sacrificato tutto per Ravka con la convinzione che saresti stato tu il giusto re e poi in un capitolo cambi idea? Eri pronto a morire per liberarti del demone ed essere il giusto re e ora non conta più nulla? La stessa folgorazione avviene per il Darkling, convinto dai tempi di Shadow and Bone di essere il salvatore di tutti i Grisha e la persona giusta per salvarli e poi alla fine decide che no, non vuole più lottare e si sacrifica volontariamente. Ma come? Un cattivo meno convinto di essere un cattivo non si è mai visto. Tutto a favore di Zoya che alla fine è un Darkling che ce l'ha fatta. Zoya alla fine ottiene quasi tutti i poteri Grisha, si può trasformare in un drago (molto hardcore, Leigh), è probabilmente immortale e diventa regina di Ravka. Poi? Niente altro? Nessuno si fa remore a mettere sul trono una persona immortale dagli infiniti poteri? Solo io vedo delle falle in questo piano? Nessuno? Ok...Infine, parliamo della storia di Nina. Nina all'inizio di King of Scars mi spezzava il cuore. Il suo lutto per Matthias e il suo dolore erano rappresentati così bene dalla scrittrice che in certi punti ho quasi pianto. Dal momento in cui arriva al convento in poi, però, il personaggio di Nina non mi ha più convinto. Come se dal momento in cui seppelisce Matthias in poi, la nuova Nina non è più un personaggio che ho trovato particolarmente impressionante o da amare. La storia con Hanna non è mai riuscita a convincermi particolarmente, tanto che nei capitoli di Nina pensavo spesso "ok bello, però cosa succede a Zoya e Nikolai?"

Con queste premesse, potreste essere portati a pensare che il libro non mi sia piaciuto. Invece si, nonostante dei difetti che gli altri libri non avevano, io l'ho amato. Complice da un lato il mio amore incondizionato per la scrittrice per cui potrebbe pure scrivere una guida su come pulire il forno e io comunque griderei al miracolo. Però il punto è che Leigh sa scrivere bene e si vede che c'è un grande lavoro dietro. RoW scritto da qualsiasi altra scrittrice sarebbe stato un calderone di roba senza senzo che mi avrebbe fatto storcere il naso. Leigh invece tiene le redini e le fila di tutti gli elementi che inserisce. Io posso non essere d'accordo sulla scelta delle trame, ma nessuna risulta mai caotica o incomprensibile. Quindi si, secondo me RoW è un bel libro e va letto. Non è al pari di Six of Crows, ma rimane comunque un ottimo prodotto letterario. Andate e leggetelo tutti. 

**No mournes, no funerals.**

Peace and love ❤️
