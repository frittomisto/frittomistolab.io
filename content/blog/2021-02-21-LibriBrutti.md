---
title:  "5 libri (soggettivamente) brutti"
date: 2021-02-21T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-02-21/2021-02-21-Copertina.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
---   

🇮🇹️ Non so se ci avete mai fatto caso, ma è sicuramente molto più facile parlare delle cose che non ci piacciono piuttosto che di quelle che amiamo. Troviamo infiniti aggettivi per descrivere qualcosa che odiamo mentre *bello* è probabilmente l'unica cosa che ci viene in mente quando consigliamo qualcosa che ci è piaciuto. Visto che io non sono da meno e non mi distinguo dalla massa, oggi sono qui per parlare di cinque libri letti nell'ultimo periodo che per me sono stati un enorme e gigante NO.

<!--more-->

Facciamo qualche premessa prima di cominciate. La prima, quella di rito, è che nessuno se la deve prendere a male se cito qualche libro che voi invece avete amato. Il mondo è bello perchè è vario e quello che non piace a me non deve non piacere a voi e vicevera. Non sono qui per giudicare voi che avete amato un libro che io reputo brutto, non sono qui per giudicare nessuno in realtà, quindi prendiamo tutti le mie parole con l'ironia e la leggerezza che meritano. Seconda premessa, alcuni libri che citerò fanno parte di una serie, quindi sebbene non ci saranno spoiler sul libro in questione ce ne saranno inevitabilmente sui libri precedenti nella serie di cui fanno parte. Ultima premessa è che l'ordine dei libri è assolutamente casuale, non rappresenta una classifica. Anche perchè mi sembrerebbe di sparare sulla crocerossa a fare una classifica dei libri brutti.

### 1. Echi in tempesta (libro 4 della saga dell'Attraversaspecchi)

![image](../../images/post/2021-02-21/2021-02-21-Attraversaspecchi.jpg)

La delusione per questo libro era già stata ampiamente annunciata nel terzo della saga, ma io onestamente ci avevo sperato fino alla fine. Ovviamente come al solito mai una gioia. Questa saga era partita benissimo, la storia e il mondo costruito dalla Dabois nei primi libri era qualcosa di incredibilmente originale e interessante. Personalmente amavo anche i personaggi di Ofelia e Thorn e la loro chimica/non chimica. Paradossalmente mi piacevano molto di più quando non si sopportavano piuttosto che da innamorati, ma va beh anche da innamorati potevo apprezzarli. Peccato che secondo me la Dabois non aveva bene in mente dove far navigare la nave di questa storia quando è partita e non solo si è persa in mezzo all'oceano strada facendo, ma si è anche completamente affossata nell'ultimo libro.

Il modo migliore per descrivere *Echi in tempesta* è confusionario, la Dabois doveva in teoria tirare le fila delle infinite storie che aveva imbastito, ma invece decide di aprire nuove trame, nuovi scenari per poi chiuderne in maniera affrettata e roccambolesca solo alcuni. Ofelia passa da essere il personaggio goffo e "spezzato" ad essere un agilissima fuggiasca che salta per i tetti. Ma va bene, questo glielo potevo pure perdonare alla Dabois. Le posso pure perdonare il finale, non mi è piaciuto ma quello è probabilmente anche dovuto al fatto che arrivata alla fine del libro ero piuttosto arrabbiata per tutte le 500 pagine precedenti. Quello che non le posso perdonare, a parte il non aver dato una spiegazione vera a tutto il casino che aveva creato, sono come ha deciso di trattare i personaggi secondari. Che senso ha crearne così tanti e dedicargli così tanto spazio per poi farli svanire in un nulla di fatto? Che senso ha, soprattutto, dedicare in questo libro interi capitoli dal punto di vista di un personaggio che poi è inutile? Mezza trama del libro completamente inutilizzata, inutile, spazio dato in beneficenza. Per non parlare di tutto quello spazio dedicato nei libri precedenti che alla fine si rivela in un nulla di fatto. Per tutte queste ragioni onestamente non saprei neanche se consigliare la serie o no. Consiglio la serie composta dai primi due libri, poi potete abbandonare e leggere altro.

### 2. Crave

![image](../../images/post/2021-02-21/2021-02-21-crave.jpg)

Su questo capolavoro assoluto della letteratura moderna non mi ci soffermo troppo perchè ci ho dedicato un'intera recensione
solo per lui [qui]({{< ref "/blog/2021-02-17-Crave.md" >}}).

Se lo meritava, effettivamente.
Sono sarcastica, ovviamente.

### 3. Blood & Honey (libro 2 della saga di Serpent & Dove)

![image](../../images/post/2021-02-21/2021-02-21-BloodHoney.jpg)

Sarebbe onesto ammettere che il primo libro della saga non mi aveva entusiasmato. Serpent & Dove è un libro secondo me pieno di difetti, cominciando inanzitutto dai personaggi. Reed l'ho trovato ottuso e misogino (nonostante le attenuanti dell'educazione ricevuta) quindi non mi era ben chiaro per quale motivo Lou finisca per innamorarsi di lui. Anche lui, d'altra parte, non ha tanti motivi per amare lei visto che rappresenta tutto quello che odia e disprezza. Però, essendo un romance, era difficile andare avanti senza la storia d'amore quindi effettivamente i due finiscono per innamorarsi e alla fine del primo libro combattono tutto e tutti per ritrovarsi insieme.
Quindi il primo libro è un romance non particolarmente originale e interessante, una storia leggera per passare il tempo senza pretese chiudendo un occhio sulle cose che non vanno nella trama (parecchie).

Nel secondo libro sarete contenti di sapere che se anche volessi fare uno spoiler non potrei perchè non succede assolutamente nulla. Le uniche scene in cui la storia va avanti sono negli ultimi due capitoli del libro perchè se no col cavolo che i lettori erano tentati di comprare il terzo. Logica editoriale del libro ponte a parte, in questo libro i problemi del primo non solo non vengono superati, ma vengono riproposti e se ne aggiungono di nuovi. Reed diventa ancora più ottuso e misogino rispetto al primo libro. Difficile ma ci riesce. Pronto a criticare e a far sentire in colpa Lou ogni volta che usa la magia per salvargli il culo. Ma quando ha deciso di amare una strega cosa si aspettava che lei rinunciasse alla magia per lui? Senza contare che è pure un ipocrita considerando che anche lui si è fatto zero remore ad usare la magia per salvare lei. Lou, d'altro canto, non aiuta molto nel diventare la versione peggiore di se stessa. Non perchè usa la magia, ma perchè tiene il piede in due scarpe tra "voglio scappare da mia madre e non ne voglio sapere nulla quindi non stressatemi" e "solo io posso sconfiggere mia madre quindi bisogna fare come dico io". Odiosa è probabilmente la parola giusta per descriverla. Ciliegina sulla torta, ad un certo punto vengono inseriti nuovi personaggi fantastici, tipo lupi mannari e sirene. Assolutamente inutili, ma perchè non aggiungere nuovi elementi magici a caso? Per finire, Beau rimane inutile tanto quanto nel primo libro, mentre se Ansel nel primo aveva avuto una qualche utilità in questo libro la perde per sempre. Good job.

### 4. An enchantment of ravens

![image](../../images/post/2021-02-21/2021-02-21-Ravens.jpg)

Romanzo d'esordio di Margaret Rogerson (ho parlato [qui]({{< ref "/blog/2021-01-17-Sorcery_Of_Thorns.md" >}})) del suo secondo romanzo) e infatti secondo me si nota che lo stile è abbastanza acerbo. La storia è quella di Isobel, una ragazza umana pittrice che riceve la commissione di dipingere il ritratto del principe dell'autunno Rook. Rook fa parte dei fair folk, una specie quasi immortale di fate che non può in nessun modo manipolare le creazioni umane. Peccato che Isobel fa l'errore di dipengere Rook con uno sguardo di umana tristezza, mettendo così in pericolo il suo potere quando il ritratto viene mostrato alla sua corte.
Deciso a farle cambiare il dipinto, Rook rapisce Isobel, ma nel tragitto verso la corte d'autunno i due si innamorano e sono costretti a sfidare tutto e tutti perchè l'amore tra un umana e un fair folk è proibito e bandito dal re delle fate.

La storia in se non sarebbe neanche male, ma è trattata in maniera estremamente superficiale. Empatizzare o apprezzare i personaggi è praticamente impossibile perchè non vengono quasi per nulla caratterizzati. Inoltre, Rook e Isobel sono il next level di innamorarsi a prima vista (sindrome di Stoccolma a parte per Isobel). Si scambiano si e no quattro frasi e sono talmente innamorati che secondo me anche a loro sembra incredibile. Infine, lo sviluppo della storia non è onestamente particolarmente interessante, sono arrivata alla fine più perchè non mi piace lasciare le cose a metà che per altro.

### 5. A curse so dark and lonely (libro 1 della serie Cursebreaker)

![image](../../images/post/2021-02-21/2021-02-21-ACurseSoDarkAndLonely.jpg)

Questo romanzo dovrebbe essere il retelling della storia della Bella e la Bestia ed è il primo di una serie di tre romanzi. Nell'impero di Emberfall il principe Rhen è stato maledetto da un'incantatrice a rivivere il suo diciottesimo anno di vita dove, alla fine dell'anno, si trasforma ogni volta in una bestia diversa che non riconosce niente e nessuno. Trecento anni dopo l'inizio della maledizione, Rhen è diventato praticamente un'eremita nel castello, ha ucciso tutta la sua famiglia e ha tentato di suicidarsi innumerevoli volte.
Gli unici che gli sono rimasti vicini sono Grey, capitano delle guardie, e Lilith, l'incantatrice sadica che si diverte a torturarlo. Grey è l'unico che può viaggiare nel mondo degli umani, il nostro mondo, dove ogni anno rapisce una ragazza nel tentativo che si innamori di Rhen e spezzi la maledizione (anche qui, sindrome di Stoccolma dietro l'angolo).

In uno di questi viaggi, Grey rapisce per sbaglio Harper, una ragazza di Washington DC che dire che ha una storia familiare tragica sarebbe minimizzare. Il padre se ne è andato lasciando la famiglia piena di debiti, il fratello fa lo strozzino per ripagare i debiti e la madre è malata di cancro e prossima a morire. Un'allegria insomma. Harper viene portata nel mondo di Emberfall e all'inizio cercherà in tutti i modi di scappare e tornare a casa, peccato che per Rhen è l'ultima possibilità per spezzare la maledizione e le cose si complicano quando un regno rivale cerca di invedere Emberfall. Questo libro poteva diventare un libro carino e fatto bene se la storia si fosse sviluppata come storia d'amore tra Rhen e Gray con Harper magari come personaggio forte che trova il suo scopo in altro. Perchè il rapporto tra Rhen e Grey si presta benissimo ad essere una storia d'amore, molto di più di quello tra Rhen e Harper. Invece no, occasione sprecata. Invece, Rhen è un aspirante suicida che più che l'amore sogna la morte, Grey è un rapinatore di ragazze (molte delle quali non tornano vive, ma la cosa sembra non toccare nessuno) e Harper è un personaggio senza senso alcuno. Tutto il libro si basa su assurdità varie e giochi di parole che meriterebbero una riga della trama, ma su cui invece si decide di basare tutta la storia. Onestamente, l'unico personaggio interessante è Lilith quando fa la sacerdotessa sadica, ma non è sicuramente abbastanza per apprezzare un intero romanzo.

Peace and love ❤️
