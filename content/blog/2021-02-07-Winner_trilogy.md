---
title:  "The Winner's Trilogy"
date: 2021-02-07T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-02-07/Copertine.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review" 
  - "young adult"
  - "fantasy"
  - "Marie Rutkoski"
---   

🇮🇹️ The Winner's Trilogy è una serie fantasy di Marie Rutkoski pubblicata nel 2016 e già abbondantemente tradotta e pubblicata anche in Italia. Ho letto questa serie con soli 5 anni di ritardo (che è comunque un ritardo inferiore di certi miei appuntamenti) e posso dirvi che questi libri hanno un solo ma gigantissimo e imperdonabile problema. Finiscono. E uno li rilegge pure, ma poi finiscono di nuovo. How dare you do this to me? Ma parliamo con calma della serie, come sempre senza spoiler alcuno.

<!--more-->

La trilogia viene classificata come una serie fantasy young adult, anche se poi l'unica componente fantasy è l'ambientazione in un mondo che non esiste. A parte questo però, non aspettatevi fate, fatine, folleti, draghi, demoni o qualsivoglia creatura magica. Non ce ne sono. Zero. Nada. Nisba. Ma soprattutto non aspettatevi quello che la copertina di questi libri rappresenta. Non vorrei offendere i fans di Classandra Clarke, ma mi sembrano delle copertine adatte per quei generi di libri, una versione per teen di un harmony fantasy, uno di quei libri che uno legge per passare il tempo, ma senza troppe aspettative. Ecco, a-s-s-o-l-u-t-a-m-e-n-t-e no. Le copertine non rispecchiano per niente quello che sono i libri e la cosa mi fa imbestialire oltremisura. Perchè io sono una di quelle persone che si fa attirare e condizionare tanto dalla copertina (e tanti cari saluti a *le apparenze non contano*) e l'abisso tra copertina e contenuto in questo caso è incolmabile. Voglio incontrare il grafico e farmi risarcire le probabili infinite ore di terapia che mi serviranno per riprendermi. Si, lo so, sono melodrammatica e tragica, ma dovrete imparare a conviverci così come io convivo con queste copertine brutte. Per farvi capire perchè mi arrabbio, cominciamo dalla trama.

![image](../../images/post/2021-02-07/Copertine.jpg "Perchè???")

Undici anni antecedenti agli eventi dei libri, l'impero di Valorian ha invaso e conquistato il piccolo stato di Herran rendendo tutti i suoi abitanti morti o schiavi. I Valoriani, infatti, sono completamente devoti alle idee di potere e conquista e Kestrel, la nostra protagonista, come figlia del generale che guida la milizia ha due futuri possibili davanti a se: sposarsi oppure intraprendere la carriera militare. Ovviamente il padre preferirebbe questa seconda scelta per la figlia. Ovviamente lei non ne preferirebbe nessuna delle due. Kestrel infatti non ha nessun interesse per la guerra, non ha interesse nell'uccidere nessuno o nel conquistare altri popoli. In un mondo guidato dagli interessi militari, Kestrel non ne vuole sapere nulla e vorrebbe dedicarsi al suo amore per il pianoforte e la musica. Tuttavia, un giorno la ragazza si trova casualmente intrappolata in un'asta per la vendita di schiavi e uno di loro attira la sua attenzione spingendola a fare offerte per lui e infine comprarlo. Esattamente come lei, infatti, Arin ha lo sguardo di chi non si è completamente piegato ai conquistatori, ma di chi vuole sfidare il fato che la vita gli impone. Inoltre il ragazzo sa cantare, e quale migliore stimolo serve ad una ragazza che ama suonare? Il rapporto tra i due protagonisti crescerà in qualcosa di più rispetto a quello di padrona e schiavo, con conseguenze inaspettate per entrambi. Kestrel dovrà imparare che Arin non è quello che sembra e che le conseguenze del suo acquisto all'asta sono molto più grandi di quello che lei si aspattesse. Allo stesso modo Arin dovrà accettare a vedere in lei non la valoriana che crede, non la padrona che lo ha comprato, ma la donna che è. Le loro vite rimarranno intrecciate una rete di complotti politici, segreti, azione e una guerra che non risparmia nessuno. Quando la vita ti impone di scegliere tra la persona che ami e il popolo a cui appartieni, quale sarebbe la tua scelta?

![image](../../images/post/2021-02-07/World.png "La mappa del mondo di Kestrel e Arin.")

Che dire, follettine e follettini, questa trilogia ha tutto. Ovviamente c'è il filone romance che fa da cornice, ma che racchiude anche molto altro. C'è tantissima azione, guerra e rivolta. Ci sono segreti, intrighi e complotti politici che tirano le fila di tutto. I personaggi sono estremamenti complessi e descritti perfettamente. Kestrel è la protagonista *badass* che tutti noi lettori ci meritiamo di leggere e di avere. Se infatti pensate che Kestrel sia una ragazzina frivola dedita solo al pianoforte, mai frase fu più sbagliata. In una società guidata dalla guerra di cui lei non vuole fare parte, Kestrel tuttavia ha un cervello che molti si possono solo sognare, è una stratega nata che usa il suo acume per piegare gli eventi in suo favore, per salvare le persone che ama, anche se questo vuol dire ingannarle, mentirgli e allontanarle. Kestrel è un personaggio in continuo scontro (soprattutto mentale, ma non solo) tra un padre che ama, ma che non riesce a comprenderla, e un amore che è sinonimo di tradimento verso il suo popolo. Entrambi questi aspetti, seppur contrastanti, fanno di Kestrel quello che è e nel tentativo di tenere fede ad entrambi la nostra protagonista si ritrova costretta a mentire a tutti, creando un intricata rete di bugie destinata inevitabilemnte a esplodere e distruggerla.

Anche Arin è un protagonista estremamente complesso, plasmato da un invasione che gli ha portato via la casa e la famiglia, trova comunque la forza di piegarsi per undici anni per poi contrattaccare al momento giusto. La ribellione verso gli oppressori è però solo il primo passo da fare e quello che segue sarà molto più doloroso, sotto tutti i punti di vista. Il fato vuole infatti Arin a capo di una ribellione sanguinaria, un pupillo del dio della morte che lo guida e lo protegge, quando in realtà dentro di se Arin è soprattutto ancora il bambino spaventato che vede portarsi via tutto ciò che ama senza poter fare nulla per fermarlo. Arin in questo mi ricorda un po' Kaz di *Six of Crows*, il personaggio tormentato e con un passato traumatico, ma che riesce a plasmare la sue sfortune per diventare più forte. Oltre ai due protagonisti, la Rutkoski descrive e caratterizza molto bene tutti i personaggi secondari, pedine in una scacchiera che vengono posizionati nel momento giusto attorno ai protagonisti. Grandi occhi a cuore per Sarsine e la sua evoluzione nel rapporto con Kestrel e grandissimi occhi a cuore per Roshar che fornisce quella nota ironica che serve all'equilibrio del romanzo.

Lo stile di scrittura della Rutkoski secondo me è **impeccabile**. Inizialmente lento, specchio di quello che solo all'apparenza è un delicato equilibrio tra oppressi ed oppressori che pian piano finirà inevitabilmente per rompersi e ribaltarsi. Il ritmo poi aumenta, i personaggi prendono consapevolezza di loro stessi e soprattutto delle loro scelte, ne affrontano le conseguenze e combattono con tutti i loro mezzi per ottenere quello che voglio. Nel tragitto saranno costretti a tradire quelli che amano e ovviamente a perdere un po' loro stessi nel farlo, perchè non c'è guerra che risparmi mai chi ci si addentra. Con l'aumentare del ritmo degli eventi, lo stile della scrittrice cambia, veniamo rimpallati tra il punto di vista di Kestrel e quello di Arin con un crescendo che tiene con il fiato sospeso. La pecca è che probabilmente a causa di questo perderete molte ore di sonno, provare per credere.

 Io, infatti, sfido chiunque a prendere in mano uno di questi libri ed essere capace di lasciarlo senza averlo finito. Se siete un minimo appassionati di complotti politici, relazioni che non sono assolutamente *lei vede lui, lui vede lei e vissero per sempre felici e contenti*, se siete appassionati del dualismo intrighi e romanticismo, allora questi libri fanno per voi. Prendete, leggeteli e poi tornate qui. Possiamo lamentarci delle copertine brutte insieme.

Peace and love  ❤️
