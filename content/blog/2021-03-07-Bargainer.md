---
title:  "The Bargainer series"
date:  2021-03-07T10:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-03-07-Bargainer.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "romance"
  - "erotic"
  - "siren"
  - "fantasy"
---

🇮🇹️ Ultimamente sto leggendo troppi romance anche per i miei standard, il che vuol dire che sto leggendo solo romance. La serie del Bargainer di Laura Thalassa è una serie fantasy di tre libri e mezzo (tre libri e una novella) del 2016 che amazon continuava a mostrarmi tra i consigliati da circa tre mesi finchè alla fine non ho ceduto e ho letto. Ammetto che l'unico motivo per cui ho ceduto è perchè la protagonista è una sirena e io, esattamente come un marinaio che si fa ammaliare, le ho sempre trovate creature affascinanti. Questo comunque non è stato sufficiente per amare la serie.

<!--more-->

Non voglio dire che la serie sia brutta perchè ho letto di peggio (soprattutto ultimamente), ho anche letto qualche altre libro della scrittrice e questa serie mi è sembrata la cosa più valida tra tutte, però da qui a dire che è una bella serie ci sono 50 sfumature di altri libri molto più belli. Partiamo dalla trama però, se no non si capisce nulla.

Callypso è una sirena sedicenne, ma niente coda da pesce, non immaginatevi Ariel. Essere una sirena vuol dire essere un essere umano qualsiasi con però il potere magico di riuscire ad ammaliare le persone, toglierle (momentaneamente) il libero arbitrio e obbligarle a fare quello che la sirena comanda. Così come nella mitologia classica le sirena ammaliavano i marinai con la loro voce, allo stesso modo quando Callypso vuole usare i suoi poteri, richiama la sirena che è dentro di lei e che tiene costantemente a bada, la sua pelle si illumina (Edward Cullen, sei tu per caso?) e l'essere umano oggetto della sua attenzione è obbligato a seguire i suoi comandi. Nessuno è immune al potere di una sirena, nè gli umani, nè gli stregoni, neppure i mutaforma, tranne i fae, creature fatate che vivono nell'Otherworld. Callypso cresce alla mercè di un patrigno che abusa sessualmente di lei per un numero non ben precisato di anni fino a quando una sera la ragazza cerca di tenerlo lontano con una bottiglia rotta e finisce inavvertitamente per ucciderlo. Molti direbbero legittima difesa, ma il patrigno è una figura potente nella società in cui vive Callypso e la ragazza, rimasta sola e spaventata da quello che ha fatto, invece che rivolgersi alle autorità, cerca l'aiuto del Bargainer per risolvere il problema. Il Bargainer, alias per Desmond Flynn, è un fae potente, una creatura dalla cattivissima reputazione che elargisce favori in cambio di favori futuri. Se chiedi un favore al Bargainer allora un favore sarai costretto a ricambiare e qualsiasi cosa lui ti chiederà sarai costretto a farlo o a morire. Con questa premessa, facciamo un salto a circa 8 anni dopo l'accaduto e quello che sappiamo è che negli otto anni passati Callypso ha collezionato più di 300 favori da restituire al Bargainer e che però i due non si vedono e non si sentono da 7 anni. Callypso non è più una ragazzina, ma una donna che usa il suo potere per condurre un'agenzia investitigativa insieme all'amica sacerdotessa. Tutto sembra andare per il meglio, quando una sera il Bargainer si presenta a casa di Callypso per cominciare a riscuotere i suoi favori. All'inizio sono favori innocenti come un bacio, una verità o una penitenza, ma poi scopriamo che in realtà Desmond ha bisogno dell'aiuto di Callypso per qualcosa che sta succedendo nell'Otherworld. Fae maschi stanno scomparendo da tutti i regni mentre fae femmine scompaiono per ricomparire dentro una bara trasparente in uno stato di perenne sonno (stile Biancaneve) con un neonato al seguito. Creepy è dire poco. Mentre Desmond e Callypso cercono di fare luce sul mistero, la scrittrice alterna capitoli ambientati otto anni prima in cui scopriamo come e perchè Callypso arriva ad avere così tanti favori non riscossi e come i due si sono separati. Non rivelo niente altro perchè altrimenti vi spoilero la serie, ma ovviamente essendo un romance aspettatevi la storia d'amore tra i due protagonisti. E neppure una storia d'amore qualsiasi, ma quella tra due soulmate (Sarah J. Maas e i suoi mate si sono estesi a macchia d'olio negli anni).

Onestamente, la trama secondo me è un molto interessante, ci sono tanti elementi che rendono la serie piena di potenziale. A partire dal personaggio di Callypso, una ragazza vittima di abusi, che continua a lottare e a soffrire per sfuggire al suo passato, ma che allo stesso tempo ha il potere dentro di se di ammaliare le persone. Ecco l'idea bella, ma la realizzazione meh, un po' meno bella. Partendo proprio dalla storia degli abusi che influenzano Callypso con qualche incubo ogni tanto e poca attrattiva verso una vita sociale, ma considerando che l'unico membro della sua famiglia ha abusato sessualmente di lei da bambina e poi lei lo ha ucciso direi che sta incredibilmente troppo bene. Il personaggio di Desmond, invece, viene perennemente descritto come l'equivalente dell'uomo nero. Un essere cattivo, spietato, di cui non bisogna fidarsi, pronto a schiacciarti per poi ucciderti. Però in realtà non fa assolutamente nulla di quello che la sua fama richiede, anzi verso Callypso è addirittura gentile e quasi premuroso quindi bho, se si voleva dare un'aria di cattivo al protagonista direi che ogni tanto qualcosa di cattivo avrebbe dovuto farlo se no non si capisce.

Ma il problema principale di questa serie è che è evidente che il focus e la parte principale sia il romance dei protagonisti e tutto il resto poco importa. Va bene, non c'è nulla di male, ma tutto il contorno che la scrittrice vuole dare alla storia risulta scialbo, poco descritto e abbastanza inutile. I personaggi secondari non sono classificati per niente, neanche ci prova la scrittrice, il che è frustrante perchè alcuni hanno anche un ruolo importante nella vita dei protagonisti. Inoltre, la storia dei fae che scompaiono e del cattivo che li rapisce è una bella storia se però la si vuole sviluppare bene e in questo caso è ovvio che non è così. Quindi perchè sforzarci a leggere pagine in più inutilmente? Perchè fanno contorno? Ok, però almeno condiscimela l'insalata di contormo, non darmi solo delle foglie sul piatto, if you know what I mean. Altro elemento per me di disturbo, ma questo è assolutamente personale, è che io ho un po' un odio verso la storia dei mate, soulmate e compagnia varia. Mi sembra sempre che si finisca ad avere una storia d'amore per un qualche legame superiore e imposto e non perchè effettivamente le due persone sono attratte l'una dall'altra. Ti vedo, ti trovo, ti riconosco come mate e ti amo. Ok, ma almeno conosciamoci un po' prima. E se la tua anima gemella è un pluriomicida maniaco? se la sua band preferita sono gli One Direction? che fai allora lo/la ami lo stesso a prescindere perchè siete soulmate? Non condivido questa policy, sorry.

Secondo me se siete fan della Maas, se siete fan di storie d'amore tipo soulmate, se amate i libri dove ci sono cosini che amano cosine e il resto poca importa, allora è la serie che fa per voi. Anche la novella aggiuntiva, scritta dal punto di vista di Desmond, vale la pena di leggerla perchè svela il punto di vista del protagonista maschile e la sua storia prima di incontrare Callypso. Però, se oltre al romance cercate altro in un libro allora lasciate perdere e non leggeteli perchè troverete poco altro in questa serie.

Peace and love ❤️
