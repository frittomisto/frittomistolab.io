---
title:  "The Wolf and the Woodsman"
date:  2021-06-27T00:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-06-27/2021-06-27-Copertina.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "english review"
  - "fantasy"
  - "adult fantasy"
  - "religion"
  - "mythology"
---      

🇺🇸️  The most common story told in the literature is the one between two persons of opposite groups that should hate each other but end up loving each other. 
This is the kind of love that must fight against everyone and everything (most of the time even the prejudice of the lovers itself) and could end really tragically (Romeo and Juliet's story affected more than one generation) or really beautifully (like Kestrel and Arien in the [Winner's Curse trilogy]({{< ref "/blog/2021-02-07-Winner_trilogy.md" >}}). 
I thought that *The Wolf and the Woodsman* was another love story of this kind, but I ended up being utterly wrong.

<!--more-->

The debut novel of Ava Reid is a book about the oppression and marginalization of several ethnoreligious minorities. It is a story about how the strength of a nation is built on the fundaments of dystopian propaganda, religious persecution, and cultural genocide of those minorities. Indeed, the religious and violent themes characterize the book as **grimdark** (most definitely not romance, even if the love story is present). Intertwined with the political allusion is a wordbuilding of magic and mythological themes inspired by Hungarian history and Jewish mythology. The fantasy setting of the book is provided by the presence of dark magic, macabre scenes, and general nature folklore. But let's talk about the story.

![gif](../../images/post/2021-06-27/story.gif)

Evike lives in a pagan village in the middle of the forest where every couple of years a wolf-girl, named for the wolf capes they wear, is sacrificed to the Woodsmen and brought to the capital city of Kiraly Szek. No one knows what happens to the girls, because no girl ever comes back to tell. 
The pagans believe and practice the magic of the old gods, while the people of Regorszag follow the Patrifaith and believe in only the Godfather of life and Godfather of death. The sacrifice of the wolf-girls allows the pagans to live mostly peacefully (if you do not consider the fear of being taken by a Woodsman every once in a while) without the fear of being attacked by the Regorszag. The mother of Evike was a victim of this sacrificing ritual and fifteen years later the same destiny seems to happen to Katalin, the young seer of the village and basically Evike's bully. 

> “The Woodsmen,” she gasps. “They're coming for you.”

Evik is the only wolf-girl without any of the three magic skills (fire making, healing or forge metal). Her bloodline is contaminated because her father was a Yehuli tax collector, making Evike a minority inside the minority itself and thus vexed by the other wolf-girls. Having no magic ability whatsoever, Evike is betrayed by her village, disguised into Katalin, and delivered to the Woodsmen for the sacrifice. During the travel to the capital, however, the group is attacked by monsters from the forest and only Evike and the Woodsmen one-eye captain survive the attack. The captain reveals himself as Barany  Gaspar, the true-born, but the outcast, prince of Regorszag, son of a foreign queen, and of two nations that are constantly at war. 

> “Te nem vagy taltos,” he manages, eye wide as he takes in the sight of me, chestnut-haired, unmasked. **You are not a seer.**  
“Te nem vagy harcos,” I shoot back between ragged breaths. **You are not a warrior.**

The outcast prince is opposed by his bastard brother, a fanatical follower of the Patrifaith religion who is gathering followers to eradicate all the minorities, pagans in the forest as well as Yehuli citizens in the capital, and become king. To save the city from the threat of a bloody tyrant, Evike and Gaspar are forced to cooperate with each other and ensure the presence of Gaspar's father on the throne (the less between the two evils). However, the relationship between the two is not as easy as it is in theory. Years of prejudices and oppressions cannot be easily forgotten and the fragile balance between the two is completely destroyed by their arrival in the capital. Once in the city, Gaspar is the submissive prince that must not upset his father or brother to maintain an appearance of peace and protect Evik from the death-destiny of all the previous wolf-girls. On the other side, Evik is half-pagan and half-Yehuli, both the two minorities are persecuted by Gaspar's people and she is most definitely tired of being the sacrificed and oppressed one. She is angry, her people are dying and have been dying for years now, but how could a wolf-girl without magic and rejected by her village save not only one but two worlds who are on the verge to be destroyed by the family of the prince that she so frighteningly start loving? (if you have read the sentence without breathing, you could now understand Evik's frustration.)

![gif](../../images/post/2021-06-27/disaster.gif)

Will Evik be able to save at least one group of her people and will the love with Gaspar be able to survive the imminent revolution? Will Gaspar be able to fight against his brother and the blind faith of his people? 

The author did an amazing job to represent the book's delicate themes of persecution and oppression of minorities. Especially the Yehuli population, the rich merchants of the city that are forced to live in a specific street and threatened with having to leave the homeland of their ancestors.

Gaspar is a character that is not easy to love. He does not easily abandon his tradition and most of the time he is not able to understand Evik's point of view. With a life as a persecutor, it is not easy to understand the struggles and frustrations of the persecuted, but it seems like most of the time Gaspar does not even try to do so. His main goal is to avoid what for sure will be the bloody reign of his brother and the easiest way to do it is to maintain the status quo of the situation. Little it matters to him that the current situation is based on the fear and repression of several different minority cultures. In the end, there is not a complete redemption of the character, but a gradually understanding of the necessity to change the situation for everyone to avoid a civil war.

In conclusion, the book of Ava Reid is a good adult fantasy, rich with mythology and religion. The romance component is just marginal to the story of Evike and Gaspar, both emarginated by their society and eager to prove something, especially to themselves. This is a book about different faiths and different people, constantly at war with each other, that must find a way to coexist to avoid their extinction. If Evike and Gaspar will be able to move forward from the dichotomy wolf-girl and Woodsman, will their people be able to do the same?

Peace and love ❤️
