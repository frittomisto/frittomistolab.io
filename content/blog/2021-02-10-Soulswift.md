---
title:  "Soulswift"
date: 2021-02-10T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-02-10-Soulswift.jpeg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "italian review"
  - "young adult"
  - "fantasy"
  - "religion"
---   

🇮🇹️ Soulswift è il secondo romanzo di Megan Bannen, pubblicato a Novembre dello scorso anno e non ancora tradotto in Italia (ma l'inglese è molto easy e alla portata di un livello base/intermedio). Io devo dire che sono stata principalmente attratta dalla copertina (perchè l'autrice non la conoscevo) e dal desiderio dopo tante saghe iniziate di leggere un fantasy stand-alone. Qui di seguito come al solito la mia non richiesta opinione spoiler free a riguardo.

<!--more-->

Da appasionata di libri fantasy quale sono io, gli stand-alone hanno sempre un certo fascino per me. Sono consapevole del fatto che il rischio che non mi piacciano sia più alto perchè costruire un nuovo mondo, magari un intero sistema magico e dei personaggi con un certo spessore richiede spazio, nel senso fisico di pagine. Quindi o sei Samantha Shannon e scrivi quello che più che libro può essere considerata un'arma di distruzione di massa di 1021 pagine, oppure ti pieghi all'evidenza che più di un libro è necessario e dai il via ad una saga. Ma le saghe hanno più di un rischio dietro l'angolo. Qualcuno forse potrà mai dimenticare Martin e Game of Thrones? Qualcuno potrà mai perdonarlo? Oppure parliamo di Goodkind e dei suoi venti e passa libri su The Sword of Truth? Bellissimi per carità, i primi quattro li ho letti uno dietro l'altro, ma venti libri sono troppi anche per me, il che è tutto dire. Oppure parliamo di Paolini che pubblica Eragon quando io ho 12 anni e finisce di pubblicare Inheritance che ne ho 20 e che quindi inevitabilmente diventa per me una delusione infinita. Guardiamo in faccia la realtà, le saghe fantasy fanno paura al lettore e allo stesso tempo sono una droga e una necessità da cui è difficile separarsi. Quindi si, di tanto in tanto apprezzo chi scrive dei fantasy stand-alone.

Soulswifter racconta la storia di Gelya, una ragazza cresciuta in un monastero in quanto sacerdetossa devota al culto di quello che per la sua religione è l'unico vero dio. Gelya non solo venera il dio, ma fa anche da tramite per portare nel mondo umano la sua voce attraverso il canto. Gelya è infatti una dei Vessel (che in questo caso secondo me potrebbe essere tradotto come canale) del dio. Secondo la religione di Gelya e degli Ovinisti, all'unico vero dio si contrappone il grande demone di Elath, una divinità imprigionata dagli Ovinisti secoli addietro per evitare la fine del mondo. Durante un summit politico che si tiene nel monastero in cui Gelya risiede, il destino della ragazza rimane intrecciato in quello di un soldato nemico, Tavik, devoto al culto di Elath vista come Madre che, una volta liberata, riporterà l'equilibrio e la pace nella sua terra. Quindi, ricapitoliamo, per Gelya e la sua religione Elath brutto demone cattivo che causerà la fine del mondo, per Tavik e la sua religione Elath madre divina che salverà il mondo e la sua terra. Lo sentite l'odore di guerra santa ed estremismi religiosi nell'aria? Nella sua missione per liberare Elath prigioniera, Tavik rende inavvertitamente Gelya il contenitore umano della dea. La ragazza si trova quindi costretta a seguire Tavik e ad affidarsi a lui per liberarsi dalla dea/demone che ha preso alloggio nel suo corpo. Per farlo, Gelya e Tavik dovranno riunire la dea al suo vero corpo, ripercorrendo la storia e le tappe di quegli stessi santi che secoli prima la imprigionarono. Nel percorrere questo viaggio, entrambi i due protagonisti dovranno riconsiderare tutte quello in cui credono, dovranno lottare contro quella che fino a poco tempo prima era la loro famiglia e dovranno farlo in fretta perchè il confine che separa Gelya dalla dea si assottiglia pian piano e cosa ne sarà della ragazza quando sarà del tutto scomparso?

I fantasy con la componente religiosa devo dire che sono sempre molto affascinanti. Permeati di fanatismo e maschilismo quel tanto che basta per farmi empatizzare con le protagoniste che si ribellano allo status quo. Il personaggio di Gelya è quello che in tutto il romanzo ha l'evoluzione meno scontata secondo me. Gelya è fondamentalmente quella che si direbbe una sempliciotta, non si fa molte domande, prende la vita così come le viene offerta e si crogiola nelle sue certezze. Salvo che poi ovviamente arriva il momento in cui tutte quelle certezze le vengono a mancare, la storia della sua religione che credeva di conoscere è solo una storia e come tale farcita di menzogne, quello che considerava come un padre le volta le spalle una volta diventata il contenitore del demone che teme, la sua infanzia non è quello che le hanno raccontato. Praticamente una vita fatta di bugie e miscredenze. Ci sarebbe da aspettarsi una rivolta di Gelya, una lotta contro un destino che non le sorride. Ma Gelya invece rimane un personaggio con una calma interiore incredibile, ovviamente ha qualche esplosione di rabbia, ma sono tutte molto mirate ad un torto momentaneo e prevalentemente guidate dalla dea che la abita. In un panorama letterario in cui la figura predominante è l'eroina che lotta contra un destino già tracciato e avverso, Gelya è incredibilmente controcorrente. Per questo poi ho trovato il finale del libro estremamente coerente e adatto al personaggio, opinione non condivisa a leggere le recensioni online, ma che non commenteremo per evitare spoilers. Il personaggio di Tavik invece è sicuramente molto diverso da Gelya, lui è un guerriero, la lotta con le spade è la sua preghiera e la sua missione e la sua fede sono quello che lo spingono a lottare. Ovviamente la vicinanza forzata con Gelya smusserà molte di queste sue caratteristiche, prima tra tutte il suo credo religioso. Ho trovato estremamente toccante il momento in cui sia lui che Gelya prendono consapevolezza che non importa quello in cui credono, gli dei sono comunque qualcosa di superiore rispetto a loro e nel migliore dei casi li stanno solo usando per ottenere quello che vogliono, ma poco importa, è verso loro stessi e le persone che amano che possono riporre la loro fede e speranza. Cheesy but powerful.

Nonostante tutto devo dire che però il libro non è riuscito a convincermi al 100%, l'ho trovato una lettura carina e scorrevole, ma molto semplice e poco accattivante. Nonostante la componente del fanatismo religioso, non c'è molta azione vera e propria e neanche un introspezione molto profonda. In una scala di giudizi, io ho dato 3.5/5 al romanzo perchè non è scritto male, ma non è mai scattata la scintilla. Come quando il bello di turno ti rifila il classico *scusa, non sei tu, sono io* per non farti sentire in colpa. Scusa libro, non sei tu, sono io.

Peace and love ❤️
