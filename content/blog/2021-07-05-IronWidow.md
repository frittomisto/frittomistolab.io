---
title:  "Iron Widow"
date:  2021-07-05T00:32:14+02:00
draft: false

# post thumb
image: "images/post/2021-07-05/IronWidow.jpeg"

# meta description
description: "this is meta description"

# post type
type: "featured"

# taxonomies
categories: 
  - "books"
tags:
  - "english review"
  - "fantasy"
  - "young adult"
  - "asian culture"
  - "feminism"
---

🇺🇸  What happens when **The Handmaid's Tale** meets **Pacific rim**? The *Iron Widow* is born as a sci-fi retelling of the story of the only female emperor of Chinese history. It is really difficult in 2021 to find a young adult fantasy that is well written and deals with interesting topics, and, above all, it does so in a non-trivial way. The Iron Widow is all of this and something more.

<!--more-->

In the last period, I am developing a (probably insane) obsession with auto-destructive characters. There is something deeply fascinating with characters who want to destroy everything and kill everyone just because the world pisses them off and they have no more patience. **I love it**. I have loved **Rin** in [The poppy war]({{< ref "/blog/2021-06-06-Poppy_War.md" >}}) and I love **Zetian** in the Iron Widow. The more the world tries to force them into a predefined box where women must stay and be obedient, the more they want to destroy (not just metaphorically speaking) the society and its preconceptions. It is an ultimate destructive feminist revolution.

![image](../../images/post/2021-07-05/feminist.gif)

> *Female*. That label has never done anything for me except dictate what I can or cannot do. No going anywhere without permission. No showing too much skin. No speaking too loudly or unkindly, or at all, if the men are talking. No living my life without being constantly aware of how pleasing I am to the eye. No future except pushing out son after son for a husband, or dying in a Chrysalis to give some boy the power to reach for glory.

Zetian lives in a world where women are considered mere objects that should be used and sacrificed without remorse. The human world has been invaded by an alien species, the Hundus, and humans are fighting for their survival outside the Great Wall in robot war machines called **Chrysalis**. A man and a woman are necessary to pilot the Chrysalis in a delicate balance between yin and yang.

![image](../../images/post/2021-07-05/PacificRim.gif)

> Yin and yang represent the opposite forces that churn the universe into life. Yin is everything cold, dark, slow, passive, and feminine. Yang is everything hot, bright, fast, active, and masculine.

Nobody cares that often the women do not survive the mental effort and die because of the strain. The sister of Zetian was one of the sacrificial lambs who died as the female pilot of the Nine-Tailed Fox. The Chrysalis is piloted by Yang Guang, one of the strongest male pilots of the world with a spirit pressure of over six thousand. Thus, it is no surprise that the female pilots of his Chrysalis are overwhelmed and crushed by his mental power, resulting often in their death. Only a Balanced Match could prevent the death of the girl in the robot. The sister of Zetian was not powerful enough, but her parents gave her away anyway in a deadly race towards money and glory. It is no surprise, then, that Zetian is now furious. She is furious toward her family who forced her sister to enlist. She is furious toward the society that sacrificed women like her sister without even blinking. She is furious toward Yang Guang who literally killed her sister. It is precisely towards the latter that her anger can find an outlet and a mission. Revenge is now her religion. 

![image](../../images/post/2021-07-05/HandmaidTale.gif)

Zetian is preparing to leave her family and city, leave her doomed love with Yizhi, one of the richest boys in the city, to become a female pilot and kill Yang Guang. Her mission, if successful, will result in her death and the dishonor of her family for generations. Does she care? Probably a little bit, but still revenge is her choice. 

> I have no faith in love. Love cannot save me. I choose vengeance.

If you think that the choice of Zetian is a suicidal mission that is doomed from the beginning, you will not be too far from reality, but you do not know Zetian. Everything seems lost for her when an unexpected attack from the Hundus results in an unwilling Zetian tied and carried by force into the yin seat of the Nine-Tailed Fox. Will her destiny be the same as her sister and countless other women? Not this time. Zetian not only survives on the battlefield but she is able to kill Yang Guang in the same way as women usually die. Now, she is the **Iron Widow**, a woman who sacrificed her male partner to power up the Chrysalis, instead of the other way around.

![image](../../images/post/2021-07-05/nightmare.gif "Be their nightmare, Wu Zetian.")

Of course in a society like the one where Zetian lives, the murderer of one of the most popular and powerful pilots by the hand of a peasant girl is a big deal. Zetian got what she wanted and avenged her sister, but is she really ready to die? She was not ready in the Nine-Tailed Fox and she is not ready now when the world seems to expect only this from her. Indeed, her punishment for killing Yang Guang is to become the partner of Li Shimin, pilot of the Vermilion Bird, also known as the **Iron Demon**. Li is the strongest human pilot, with a spiritual force of over ten thousand, but, contrary to Yang Guang, he is not loved. He is a criminal, an assassin who killed his brothers and father and all the other female pilots that were paired with him. No one survives more than one battle with him and Zetian is next in line. Now her only choice is to kill or be killed.

The Iron Demon, however, is not what she thinks. What if Li is a victim as much as all the girls he has killed? What if it is not the pilot to be blamed but the society that so easily has sacrificed its pilots? What will happen if the two most hated, controversial and powerful pilots are able to force a Balanced Match? Will this delicate equilibrium survive the presence of Yizhi, arrived in the compound to help Zetian? 

I was honestly scared that the writer would choose to pursuit the topos of the love triangle, but thank God this is not the case. Instead, the (probably bold) choice of Xiran Jay Zhao is a **polyamorous relationship,** perfect for the plot of this book. Li, Zetian and Yizhi are the perfect balance between the gentle touch and soul of Yizhi, the destructive force and anger of Zetian and the strong but broken willpower of Li.

![image](../../images/post/2021-07-05/tringle.gif)

I was initially skeptical about this book because the sci-fi is not my preferite genre, but I quickly changed my mind. The sci-fi part of the book is kept to a minimum and it is perfectly integrated with the story of Zetian and her quest against the world. The main focus is the feminist emancipation that Zetian wants to achieve  for herself all the other women, dead and alive. Her methods are totally destructive, there is no space for words, dialogues or peaceful resolution. She wants to destroy everything and everyone with the same lack of consideration that her world reserved to all the women, past and present. Gandhi would probably not be the greatest fan of Zetian, but she would not care anyway to have his acceptance. 

> Redemption story, they said? There will be no redemption. It is not me who is wrong. It's everyone else.

This book was honestly one of the best mind-blowing reading of this year. It is a letal cocktail of destruction, feminism and unconventional love that kept me glued to the pages. The cliffhanger ending did not bother me too much, on the contrary I am really curious to see where the choice of Zetian will take her on the next book.

Peace and love ❤️