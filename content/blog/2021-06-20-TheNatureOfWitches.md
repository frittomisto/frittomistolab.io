---
title:  "The nature of witches"
date: 2021-06-20T10:07:47+06:00
draft: false

# post thumb
image: "images/post/2021-06-20_the_nature_of_witches.jpg"

# meta description
description: "this is meta description"

# post type
type: "post"

# taxonomies
categories: 
  - "books"
tags:
  - "review"
  - "young adult"
  - "witches"
---     

🇺🇸 *The Nature of Witches* is the debut Young Adult fantasy novel of Rachel Griffin. It is a novel about self-acceptance, growth, trust, love, and, mainly, climate change. It could seem strange to include a topic that is so sensitive in a YA fantasy, but the writer does an amazing job of interwave it with the plot of the book.  
 It is never too burdensome for the reader who can enjoy the book while, at the same time, realize what we are doing to the Earth.

<!--more-->

The worldbuilding of Rachel Griffin's story is a modern American city in which humans and witches coexist next to each other. The power and the personality of the witches are strictly connected to the seasons in which they are born. So, summer witches are friendly and joyful, while winter witches are tougher and more independent. Spring witches are calm and relaxed, while autumn witches are tense and volatile. During the corresponding season, the power of the witch is at full possibility, while it decreases during all the other seasons. 

![image](../../images/gif/season_gif.gif)

Clara is the only exception to this rule. She is an Everwitch, whose magic is tied to every season. Clara and her magic change as the seasons change, making her powerful during the entire year. She is supposed to be the most powerful witch of her generation, and a lot of expectations are put on her shoulders. The world indeed is changing and on the verge of disaster: winter storms during summer, sudden heat waves during winter, and tornadoes of frightening destructive force are all symptoms of the atmosphere becoming erratic.  
The witches are supposed to maintain the proper climate balance, but fighting these abnormal phenomena requires a massive power effort, going as far as causing the death of more and more witches. Clara could be the key to both save the witches and maintain the climate balance, but she does not want anything to do with her power.  
Her magic is responsible for the death of her parents and her best friend because when she loses control over her power, it attacks those emotionally connected to Clara, causing their death in extreme circumstances. Clara thus condemned herself to a solitary life, in a drastic attempt to keep safe those she loves. In this not-so-happy scenario, one day a recently graduate student joins the school of Clara to help her control her magic. Sang is a spring witch and a botanist with a particular affinity to calm magic. Where Clara is frustrated and full of resentment towards her powers, Sang is the exact opposite and wants to show her how to trust and control her magic. As the connection between the two starts growing, so Clara learns to let her magic flow and maybe becomes the great witch that the world expects her to be. The connection with Sang, however, makes him the perfect target to Clara's magic and she does not want to cause any more deaths, especially not to him. The only possible choices to Clara are to embrace her power, thus trying to save the witches, but isolate herself from everyone or to give up on her magic, possibly condemning more and more witches to die, to be around those she loves as a normal person rather than a witch.  
Both two choices required great sacrifices, will she be able to make them or find a third opportunity?

I honestly really enjoyed the tangle of magic and nature, the way to deliver the climate change message of the author was really good. Clara is a character that is not easy to love, her frustration becomes the reader's frustration and her choice of isolation easily turns her into an egoistic and bratty personality. 

![image](../../images/gif/frustration_gif.gif)

I think the main emotions during the read of this book are frustration/anger and sadness, but, even if it seems something bad, I think it is a conscious choice of the writer. Clara's emotions are the reader's emotions and what is more powerful than to get so in tune with the book's character?

![image](../../images/gif/sadness_gif.gif)

Clara lost her parents and her friend, her power killed them and so she feels their death on her hands. How can she possibly embrace the same power that brings her so much sadness? People around her continuously tell her that she is strong, that is her responsibility to save everyone, and that she is not trying enough to become who she is meant to be. With so much pressure on her shoulder and so much pain in her past, she is constantly on the brick of explosion and only a character like Sang could save her from herself.

Sang feels a little bit like the zen master, the nerd botanist who loves plants and flowers. Not the usual Latin lover description, but it works for Clara. Their love starts and grows as the seasons change because everything in the book is subjugated to the climate cycle. For this reason, the main fear of Clara becomes the destiny of her love for Sang as summer passes and autumn approaches. Clara is bind to change with the season and, usually, this entails less loving emotion during autumn and winter seasons. Does the love between Clara and Sang survive the approach of the cold seasons or will it melt like snow under the sun?

![image](../../images/gif/olaf_gif.gif)

If you like tormented character with a lot of emotional baggage, if you enjoy the personal quests of finding yourself, if you like a nice and warm love story, and, of course, if you like nature and you are a Greta Thunberg fan, then this is the book for you. It is an easy and nice reading that will let you think about what we are doing as humans to the Earth. Remember that there are no season witches out there that are going to save us, maybe we should all be like Clara and embrace our power to try to keep the climate balance.

Peace and love ❤️
